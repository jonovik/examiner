test_that("translate_text works", {
  expect_equal(translate_text(text = "Hello, how are you?", langpair = "en|es"),
               "Hola, cómo eres?")
  expect_equal(translate_text(text = "God morgen, hvordan har du det?", langpair = "nob|nno"),
               "God morgon, korleis har du det?")
})

test_that("translate_text error lists pairs", {
  expect_error(translate_text(text = "God morgen, hvordan har du det?", langpair = "no|nn"),
               "Available pairs are")
})

test_that("translate works for question file", {
  expect_no_error(question_file("multiple_choice_question") |>
                    readr::read_file() |>
                    translate_text(langpair = "en|es"))
})

test_that("translate works for quiz file", {
  expect_no_error(r"(---
title: Test translation of quiz
output: examiner::test_knit
questions:
  - {question_file("multiple_choice_question")}
---

This quiz has been translated from English to Spanish.
)" |>
    glue::glue() |>
    translate_text(langpair = "en|es"))
})

test_that("list_pairs() works", { expect_no_error(list_pairs()) })

test_that("append_basename_lang() works", {
  expect_equal(append_basename_lang("test.txt", "en"), "test_en.txt")
  expect_equal(append_basename_lang("test", "en"), "test_en")
  expect_equal(append_basename_lang("c:/dir/test.ext", "en"), "C:/dir/test_en.ext")
})
