library(testthat)

# Multiple choice question

test_that("abstain option works in parse_answers for multiple_choice_question", {
  r"(
  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <title>Minimal Test Document</title>
    </head>
    <body>
      <div id="answerlist">
        <ul>
          <li>test</li>
        </ul>
      </div>
    </body>
  </html>
  )" |>
    xml2::read_html() |>
    parse_answers(meta = list(extype = "multiple_choice_question"),
                  abstain = TRUE) -> answer
  expect_match(answer[[2]]$answer_html, "unanswered")
})


# Numerical question

test_that("abstain option works in parse_answers for numerical_question", {
  r"(
  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <title>Minimal Test Document</title>
    </head>
    <body>
      <div id="answerlist">
        <ul>
          <li>42</li>
        </ul>
      </div>
    </body>
  </html>
  )" |>
    xml2::read_html() |>
    parse_answers(meta = list(extype = "numerical_question"),
                  abstain = TRUE) -> answer
  expect_in(answer |> purrr::map_chr("numerical_answer_type"),
            c("exact_answer", "range_answer", "precision_answer"))
  expect_no_match(answer |> purrr::map_chr("answer_html", .default = NA_character_),
                  "unanswered")
})

r"(
* 123 | Exact answer with zero margin
* 456.78 [margin: 0.04] | Exact answer with margin
* 12 - 34 | Range answer
* 3.14159 [precision: 2] | "Precision" answer (number of decimal places)
* 1234 [precision: 2] | Biggish precision answer
* 0.001234 [precision: 2] | Smallish precision answer
* 0.0001234 [precision: 2] | Small precision answer
* 0.0000001234 [precision: 2] | Tiny precision answer
)" |>
  stringr::str_trim() |>
  stringr::str_split_1("\n") |>
  tibble::tibble(x = _) |>
  tidyr::separate_wider_delim(x,
                              stringr::fixed(" | "),
                              names = c("answer_text", "description")) -> tests
# tests |> dplyr::rowwise() |> dplyr::mutate(numerical_answer(answer_text))

test_that("exact_answer with zero margin works", {
  expect_identical(numerical_answer("* 123"),
                   tibble::tibble(numerical_answer_type = "exact_answer",
                                  answer_exact = "123"))
})
test_that("exact_answer with margin works", {
  expect_identical(numerical_answer("* 456.78 [margin: 0.04]"),
                   tibble::tibble(numerical_answer_type = "exact_answer",
                                  answer_exact = "456.78",
                                  answer_error_margin = "0.04"))
})
test_that("range_answer works", {
  expect_identical(numerical_answer("* 12 - 34"),
                   tibble::tibble(numerical_answer_type = "range_answer",
                                  answer_range_start = "12",
                                  answer_range_end = "34"))
})
test_that("precision_answer works", {
  expect_identical(numerical_answer("* 3.14159 [precision: 2]"),
                   tibble::tibble(numerical_answer_type = "precision_answer",
                                  answer_approximate = "3.14159",
                                  answer_precision = "2"))
})
test_that("biggish precision_answer works", {
  expect_identical(numerical_answer("* 1234 [precision: 2]"),
                   tibble::tibble(numerical_answer_type = "precision_answer",
                                  answer_approximate = "1234",
                                  answer_precision = "2"))
})
test_that("smallish precision_answer works", {
  expect_identical(numerical_answer("* 0.001234 [precision: 2]"),
                   tibble::tibble(numerical_answer_type = "precision_answer",
                                  answer_approximate = "0.001234",
                                  answer_precision = "2"))
})
test_that("small precision_answer works", {
  expect_identical(numerical_answer("* 0.0001234 [precision: 2]"),
                   tibble::tibble(numerical_answer_type = "precision_answer",
                                  answer_approximate = "0.0001234",
                                  answer_precision = "2"))
})
test_that("tiny precision_answer works", {
  expect_identical(numerical_answer("* 0.0000001234 [precision: 2]"),
                   tibble::tibble(numerical_answer_type = "precision_answer",
                                  answer_approximate = "0.0000001234",
                                  answer_precision = "2"))
})

test_that("Answerlist lengths in Question and Solution must match", {
  fname <- tempfile(fileext = ".Rmd")
  withr::with_file(fname, {
    r"(
    # Question

    Two alternatives.

    ## Answerlist

    * A
    * B

    # Solution

    Only one feedback.

    ## Answerlist

    * The first alternative.

    # Meta-information

    extype: multiple_choice_question
    exsolution: 10
    )" |> glue::glue() |> writeLines(fname)
    expect_error(parse_question(fname),
                 "Answerlist has different length")
  })
})

test_that("Answerlist length check handles nested list items", {
  fname <- tempfile(fileext = ".Rmd")
  withr::with_file(fname, {
    r"(
    # Question

    Two alternatives.

    ## Answerlist

    * A
    * B

    # Solution

    Only one feedback.

    ## Answerlist

    * The first alternative.
    * The second alternative.

      Explanatory bullet points:

      * First point.
      * Second point.

    # Meta-information

    extype: multiple_choice_question
    exsolution: 10
    )" |> glue::glue() |> writeLines(fname)
    expect_no_error(parse_question(fname))
  })
})
