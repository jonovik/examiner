test_that("skip_banking() works", {
  rmds <- c("bank1.Rmd 1/2", "no bank.Rmd", "bank3.Rmd 3/4")
  expect_equal(skip_banking(rmds), c("bank1.Rmd", "no bank.Rmd", "bank3.Rmd"))
  rmds <- list(A = c("bank1.Rmd 1/2", "no bank.Rmd"), B = "bank3.Rmd 3/4")
  expect_equal(skip_banking(rmds), list(A = c("bank1.Rmd", "no bank.Rmd"), B = "bank3.Rmd"))
})

test_that("knit_all() works", {
  filename <- tempfile(fileext = ".html")
  rmds <- system.file(c("multiple_choice_question.Rmd",
                        "multiple_choice_question_random.Rmd"),
                      package = "examiner",
                      mustWork = TRUE)
  expect_match(readr::read_file(knit_all(rmds, filename)), "multiple_choice_question.Rmd")
  rmds_list <- list("First section" = rmds,
                    "Second section" = system.file("xhtml-features.Rmd",
                                                   package = "examiner",
                                                   mustWork = TRUE))
  expect_match(readr::read_file(knit_all(rmds_list, filename)), "Second section")
  file.remove(filename)
})
