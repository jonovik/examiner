test_that("Canvas question bank creation works", {
  skip_if_not(rstudioapi::isAvailable())
  skip_if_not_test_server()
  rmds <- c("multiple_choice_question", "xhtml_features") |> sapply(question_file)
  expect_no_error(canvas_create_static_bank(rmds))
})

test_that("canvas_create_dynamic_bank reuses existing bank", {
  skip_if_not(rstudioapi::isAvailable())
  skip_if_not_test_server()
  rmd <- tempfile(fileext = ".Rmd")
  withr::with_file(rmd, {
    file.copy(question_file("multiple_choice_question"), rmd)
    expect_no_error(url <- canvas_create_dynamic_bank(rmd, 1))
    expect_message(canvas_create_dynamic_bank(rmd, 1),
                   "Found existing bank")
    expect_no_error(canvas_delete_bank(id = url |> stringr::str_split_i("/", -1)))
  })
})

test_that("Canvas question bank deletion works", {
  skip_if_not(rstudioapi::isAvailable())
  skip_if_not_test_server()
  rmd <- question_file("multiple_choice_question")
  expect_no_error(canvas_create_static_bank(rmd, title = "Delete me!"))
  canvas_create_static_bank(rmd, title = "Delete me!")
  expect_no_error(canvas_delete_bank(title = "Delete me!"))
})

test_that("canvas_create_question can create a question group", {
  skip_if_not(rstudioapi::isAvailable())
  skip_if_not_test_server()
  quiz <- suppressWarnings(canvas_create_quiz(rmds = c()))
  rmd <- question_file("multiple_choice_question") |>
    stringr::str_c(" 2/3")
  expect_no_error(canvas_create_question(rmd))
})

test_that("compute_bank_url() works", {
  expect_match(compute_bank_url(123),
               "courses/[0-9]+/question_banks/[0-9]+$")
  url <- glue::glue(r"({Sys.getenv("CANVAS_DOMAIN")}/courses/{Sys.getenv("CANVAS_COURSE_ID")}/question_banks/1234)")
  expect_equal(url, compute_bank_url(url))
  # Not implemented: Testing for unique/nonexistent/duplicate question bank name.
})

test_that("canvas_deduplicate_bank() works", {
  skip_if_not(rstudioapi::isAvailable())
  skip_if_not_test_server()
  rmd <- tempfile(fileext = ".Rmd")
  withr::with_file(rmd, {
    file.copy(question_file("multiple_choice_question"), rmd)
    n <- 2
    url <- canvas_create_dynamic_bank(rmd, n)
    # There can be only one
    expect_equal(n - 1, canvas_deduplicate_bank(url))
  })
})
