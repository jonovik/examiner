library(testthat)

# Deterministic replacement for uuid, universally unique identifiers
mock_make_uuid <- function() {
  counter <- 0
  function() {
    counter <<- counter + 1
    sprintf("aaaaaaaa-aaaa-aaaa-aaaa-%012d", counter)
  }
}

# mock_uuid <- mock_make_uuid()
# mock_uuid()
# mock_uuid()

test_that("rmarkdown::render() generates UTF-8", {
  input <- tempfile("æøå", fileext = ".Rmd")
  writeLines("---\ntitle: æøå\n---\næøå", input)
  output <- rmarkdown::render(
    input,
    output_format = "html_fragment",
    output_options = list(
      self_contained = FALSE,
      md_extensions = list("-smart", "-auto_identifiers"),
      pandoc_args = list("--metadata=title=examiner_question",
                         "--wrap=preserve",
                         "--eol=lf")),
    envir = new.env(),
    quiet = TRUE)
  readLines(output, warn = FALSE) |>
    paste(collapse = "") |>
    expect_equal("<p>æøå</p>")
  file.remove(output)
  file.remove(input)
})

test_that("wiseflow_export_assignment handles info page", {
  # sections as named list of character vectors
  question_file("multiple_choice_question") |>
    list(section1_title = _) -> sections
  info <- "<h1>Information page generated with <code>library(examiner)</code></h1>"
  filename <- tempfile(fileext = ".json")
  output <- wiseflow_export_assignment(title = "test_with_info_page",
                                       sections = sections,
                                       filename = filename,
                                       info_page = info)
  expect_named(output, c("data", "metadata"))
})

test_that("wiseflow_export_item reports unsupported question_type", {
  withr::with_dir(tempdir(), {
    r"(
      # Question

      ## Answerlist

      * 123

      # Meta-information

      extype: this_should_cause_an_error
    )" |>
      trimws() |>
      glue::glue() |>
      writeLines("unsupported.Rmd")
    expect_error(wiseflow_export_items(list(item = "unsupported.Rmd")),
                 "not recognized")
  })
})

test_that("wiseflow_export_item reports nonexistent question_type", {
  withr::with_dir(tempdir(), {
    r"(
      # Question
      # Meta-information
      extype: nonexistent
    )" |>
      trimws() |>
      glue::glue() |>
      writeLines("nonexistent.Rmd")
    expect_error(wiseflow_export_items(list(item = "nonexistent.Rmd")),
                 "Question type.*not recognized")
  })
})

test_that("wiseflow_export_item handles multiple_choice_question", {
  with_mocked_bindings(make_uuid = mock_make_uuid(), {
    expect_no_error(it <- question_file("demo_feedback_multiple_choice_question") |>
                      list(item = _) |>
                      wiseflow_export_items())
  })
  it$metadata$date_created <- "<date>"
  expect_snapshot_value(it, style = "json2")
})

test_that("wiseflow_export_item handles multiple_answers_question", {
  with_mocked_bindings(make_uuid = mock_make_uuid(), {
    expect_no_error(it <- question_file("multiple_answers_question") |>
                      list(item = _) |>
                      wiseflow_export_items())
  })
  it$metadata$date_created <- "<date>"
  expect_snapshot_value(it, style = "json2")
})

test_that("wiseflow_export_item handles multiple_dropdowns_question", {
  with_mocked_bindings(make_uuid = mock_make_uuid(), {
    expect_no_error(it <- question_file("multiple_dropdowns_question") |>
                      list(item = _) |>
                      wiseflow_export_items())
  })
  it$metadata$date_created <- "<date>"
  expect_snapshot_value(it, style = "json2")
})

test_that("wiseflow_export_item handles fill_in_multiple_blanks_question", {
  with_mocked_bindings(make_uuid = mock_make_uuid(), {
    expect_no_error(it <- question_file("fill_in_multiple_blanks_question") |>
                      list(item = _) |>
                      wiseflow_export_items())
  })
  it$metadata$date_created <- "<date>"
  expect_snapshot_value(it, style = "json2")
})

test_that("wiseflow_export_item handles short_answer_question", {
  with_mocked_bindings(make_uuid = mock_make_uuid(), {
    expect_no_error(it <- question_file("short_answer_question") |>
                      list(item = _) |>
                      wiseflow_export_items())
  })
  it$metadata$date_created <- "<date>"
  expect_snapshot_value(it, style = "json2")
})

test_that("wiseflow_export_item handles matching_question", {
  with_mocked_bindings(make_uuid = mock_make_uuid(), {
    expect_no_error(it <- question_file("matching_question") |>
                      list(item = _) |>
                      wiseflow_export_items())
  })
  it$metadata$date_created <- "<date>"
  expect_snapshot_value(it, style = "json2")
})

test_that("wiseflow_export_item handles essay_question", {
  with_mocked_bindings(make_uuid = mock_make_uuid(), {
    expect_no_error(it <- question_file("essay_question") |>
                      list(item = _) |>
                      wiseflow_export_items())
  })
  it$metadata$date_created <- "<date>"
  expect_snapshot_value(it, style = "json2")
})

test_that("wiseflow_export_item handles file_upload_question", {
  with_mocked_bindings(make_uuid = mock_make_uuid(), {
    expect_no_error(it <- question_file("file_upload_question") |>
                      list(item = _) |>
                      wiseflow_export_items())
  })
  it$metadata$date_created <- "<date>"
  expect_snapshot_value(it, style = "json2")
})

test_that("wiseflow_export_item handles numerical_question", {
  with_mocked_bindings(make_uuid = mock_make_uuid(), {
    expect_no_error(it <- question_file("numerical_question") |>
                      list(item = _) |>
                      wiseflow_export_items())
  })
  it$metadata$date_created <- "<date>"
  expect_snapshot_value(it, style = "json2")
})

test_that("wiseflow_export_item handles text_only_question", {
  with_mocked_bindings(make_uuid = mock_make_uuid(), {
    withr::with_file("text_only_question_test.Rmd", {
      r"(
      # Question

      This is a **Text Only Question** (text_only_question) for testing.

      # Meta-information

      extype: text_only_question
      )" |>
        glue::glue() |>
        writeLines("text_only_question_test.Rmd")
      # Note that a WISEflow 'item' must contain at least one question,
      # so we cannot just test a 'passage' (= text_only_question)
      expect_no_error(it <- c("text_only_question", "multiple_choice_question") |>
                        purrr::map_chr(question_file) |>
                        list(item = _) |>
                        wiseflow_export_items())
    })
    it$metadata$date_created <- "<date>"
    expect_snapshot_value(it, style = "json2")
  })
})
