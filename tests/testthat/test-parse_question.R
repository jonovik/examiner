test_that("parse_question works", {
  expect_no_error(
    examiner:::parse_question(system.file("multiple_choice_question.Rmd",
                                          package = "examiner", mustWork = TRUE)))
})

test_that("parse_question() generates UTF-8", {
  input <- tempfile("æøå", fileext = ".Rmd")
  r"(
---
title: æøå
---

# Question

æøå

## Answerlist

* ÆØÅ

# Meta-information

exsolution: 1
extype: schoice
)" |> trimws() |> writeLines(input)
  output <- examiner:::parse_question(rmd = input)
  expect_equal(output$question_name, "æøå")
  expect_equal(output$question_text, "<p>æøå</p>")
  expect_equal(output$answers[[1]]$answer_html, "<div>ÆØÅ</div>")
  file.remove(input)
})

test_that("gganimate works in parse_question", {
  skip_if(nzchar(Sys.getenv("_R_CHECK_TIMINGS_") > 0),
          "unable to test gganimate gif under devtools::check()")
  suppressWarnings(file.remove("animation.gif"))
  rmd_name <- system.file("xhtml-features.Rmd", package = "examiner")
  tempd <- tempdir()
  tempf <- file.path(tempd, basename(rmd_name))
  file.copy(rmd_name, tempf)
  q <- examiner:::parse_question(tempf)
  expect_true(file.exists(file.path(tempd, "animation.gif")))
  unlink(tempd, recursive = TRUE)
})

test_that("Meta-information is required", {
  fname <- tempfile(fileext = ".Rmd")
  withr::with_file(fname, {
    r"(
    # Question

    No meta-information here.
    )" |> glue::glue() |> writeLines(fname)
    expect_error(parse_question(fname),
                 "Meta-information not found")
  })
})

test_that("Question is required", {
  fname <- tempfile(fileext = ".Rmd")
  withr::with_file(fname, {
    r"(
    # Not a question

    No question here.

    # Meta-information

    extype: text_only_question
    )" |> glue::glue() |> writeLines(fname)
    expect_error(parse_question(fname),
                 "Question text not found")
  })
})

test_that("Answerlist length is checked", {
  fname <- tempfile(fileext = ".Rmd")
  withr::with_file(fname, {
    r"(
    # Question

    Mismatch between Answerlist length and exsolution.

    ## Answerlist

    * A
    * B

    # Meta-information

    extype: multiple_choice_question
    exsolution: 100
    )" |> glue::glue() |> writeLines(fname)
    expect_error(parse_question(fname),
                 "nrow(answers) == nchar(meta$exsolution) is not TRUE",
                 fixed = TRUE)
  })
})
