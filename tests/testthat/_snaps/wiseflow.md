# wiseflow_export_items handles multiple_choice_question

    {
      "data": [
        {
          "definition": {
            "template": "dynamic",
            "widgets": [
              {
                "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                "widget_type": "response"
              }
            ]
          },
          "questions": [
            {
              "data": {
                "metadata": {
                  "distractor_rationale_response_level": [
                    "<div>Blood is indeed red, arterial blood more so than venous blood.<\/div>",
                    "<div>No known organisms have white blood.\nWhite blood cells do exist, but the red blood cells dominate the colour of blood.<\/div>",
                    "<div>\n<a href=\"https://en.wikipedia.org/w/index.php?title=Nobility&amp;oldid=1220366260#cite_note-21\">Royal blood was considered \"blue\"<\/a> by the ancient Spaniards, since veins appear bluish through pale skin.\nHowever, closer inspection will reveal that blood is not actually blue.<\/div>"
                  ]
                },
                "options": [
                  {
                    "label": "<div>Red<\/div>",
                    "value": "0"
                  },
                  {
                    "label": "<div>White<\/div>",
                    "value": "1"
                  },
                  {
                    "label": "<div>Blue<\/div>",
                    "value": "2"
                  }
                ],
                "shuffle_options": true,
                "stimulus": "<p>This is a <strong>Multiple Choice<\/strong> question (multiple_choice_question).<\/p>\n<p>What is the colour of blood?<\/p>",
                "type": "mcq",
                "ui_style": {
                  "choice_label": "upper-alpha",
                  "type": "horizontal"
                },
                "validation": {
                  "scoring_type": "exactMatch",
                  "valid_response": {
                    "score": 1,
                    "value": [
                      "0"
                    ]
                  }
                }
              },
              "metadata": {
                "name": "Multiple responses"
              },
              "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
              "type": "mcq",
              "widget_type": "response"
            }
          ],
          "status": "published",
          "title": "item"
        }
      ],
      "metadata": {
        "author_version": "4.2.0",
        "content": "item",
        "created_by": "library(examiner)",
        "date_created": "<date>"
      }
    }

# wiseflow_export_items handles multiple_choice_question with abstain=TRUE

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["definition", "questions", "status", "title"]
                }
              },
              "value": [
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "metadata", "reference", "type", "widget_type"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["metadata", "options", "shuffle_options", "stimulus", "type", "ui_style", "validation"]
                            }
                          },
                          "value": [
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["distractor_rationale_response_level"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "NULL"
                                    },
                                    {
                                      "type": "NULL"
                                    },
                                    {
                                      "type": "NULL"
                                    },
                                    {
                                      "type": "NULL"
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Red<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["0"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>White<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["1"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Blue<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["2"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>I wish to leave this question unanswered<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["3"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Multiple Choice<\/strong> question (multiple_choice_question).<\/p>\n<p>What is the colour of blood?<\/p>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["mcq"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["choice_label", "type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["upper-alpha"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["horizontal"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["allow_negative_scores", "alt_responses", "penalty", "scoring_type", "valid_response"]
                                }
                              },
                              "value": [
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["score", "value"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "double",
                                          "attributes": {},
                                          "value": [0]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": [
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["3"]
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  "type": "double",
                                  "attributes": {},
                                  "value": [0.5]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["exactMatch"]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["score", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    },
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["0"]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Multiple responses"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["mcq"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "content", "created_by", "date_created"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items allows penalty=TRUE as synonym for abstain=TRUE

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["definition", "questions", "status", "title"]
                }
              },
              "value": [
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "metadata", "reference", "type", "widget_type"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["metadata", "options", "shuffle_options", "stimulus", "type", "ui_style", "validation"]
                            }
                          },
                          "value": [
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["distractor_rationale_response_level"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Blood is indeed red, arterial blood more so than venous blood.<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>No known organisms have white blood.\nWhite blood cells do exist, but the red blood cells dominate the colour of blood.<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>\n<a href=\"https://en.wikipedia.org/w/index.php?title=Nobility&amp;oldid=1220366260#cite_note-21\">Royal blood was considered \"blue\"<\/a> by the ancient Spaniards, since veins appear bluish through pale skin.\nHowever, closer inspection will reveal that blood is not actually blue.<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": [null]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Red<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["0"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>White<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["1"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Blue<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["2"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>I wish to leave this question unanswered<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["3"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Multiple Choice<\/strong> question (multiple_choice_question).<\/p>\n<p>What is the colour of blood?<\/p>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["mcq"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["choice_label", "type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["upper-alpha"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["horizontal"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["allow_negative_scores", "alt_responses", "penalty", "scoring_type", "valid_response"]
                                }
                              },
                              "value": [
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["score", "value"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "double",
                                          "attributes": {},
                                          "value": [0]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": [
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["3"]
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  "type": "double",
                                  "attributes": {},
                                  "value": [0.5]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["exactMatch"]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["score", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    },
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["0"]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Multiple responses"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["mcq"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "content", "created_by", "date_created"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items handles multiple_answers_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["definition", "questions", "status", "title"]
                }
              },
              "value": [
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "metadata", "reference", "type", "widget_type"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["metadata", "multiple_responses", "options", "shuffle_options", "stimulus", "type", "ui_style", "validation"]
                            }
                          },
                          "value": [
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["distractor_rationale_response_level"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": [""]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": [""]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Royalty are sometimes said to have \"blue blood\", so we'll accept this one too.<\/div>"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Red<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["0"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>White<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["1"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Blue<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["2"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Multiple Answers<\/strong> question (multiple_answers_question).<\/p>\n<p>Which colours describe blood?\nCheck all that apply.<\/p>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["mcq"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["choice_label", "type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["upper-alpha"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["horizontal"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["allow_negative_scores", "min_score_if_attempted", "penalty", "rounding", "scoring_type", "valid_response"]
                                }
                              },
                              "value": [
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "double",
                                  "attributes": {},
                                  "value": [0]
                                },
                                {
                                  "type": "double",
                                  "attributes": {},
                                  "value": [1]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["none"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["partialMatchV2"]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["score", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["0", "2"]
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Multiple responses"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["mcq"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "content", "created_by", "date_created"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items handles multiple_dropdowns_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["definition", "questions", "status", "title"]
                }
              },
              "value": [
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "metadata", "reference", "type", "widget_type"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["possible_responses", "stimulus", "template", "type", "validation"]
                            }
                          },
                          "value": [
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["up", "down", "out"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["blue", "green", "the limit"]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": [""]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Multiple Dropdowns<\/strong> question (multiple_dropdowns_question).<\/p>\n<p>Please complete this line from \"Dear Prudence\" by the Beatles:<\/p>\n<p>The sun is {{response}}, the sky is {{response}}.<\/p>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["clozedropdown"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["allow_negative_scores", "alt_responses", "match_all_possible_responses", "rounding", "scoring_type", "valid_response"]
                                }
                              },
                              "value": [
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["score", "value"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "double",
                                          "attributes": {},
                                          "value": [1]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": [
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["up"]
                                            },
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["the limit"]
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["none"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["partialMatchV2"]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["score", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    },
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["up"]
                                        },
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["blue"]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Fill in text with drop-down"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["clozedropdown"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "content", "created_by", "date_created"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items handles fill_in_multiple_blanks_question

    {
      "data": [
        {
          "definition": {
            "template": "dynamic",
            "widgets": [
              {
                "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                "widget_type": "response"
              }
            ]
          },
          "questions": [
            {
              "data": {
                "stimulus": "",
                "template": "<p>This is a <strong>Fill In Multiple Blanks<\/strong> question (fill_in_multiple_blanks_question).<\/p>\n<p>Please complete this line from \"Dear Prudence\" by the Beatles:<\/p>\n<p>The sun is {{response}}, the sky is {{response}}.<\/p>",
                "type": "clozetext",
                "validation": {
                  "allow_negative_scores": true,
                  "alt_responses": [],
                  "match_all_possible_responses": true,
                  "rounding": "none",
                  "scoring_type": "partialMatchV2",
                  "valid_response": {
                    "score": 1,
                    "value": [
                      "up",
                      "blue"
                    ]
                  }
                }
              },
              "metadata": {
                "name": "Fill in text"
              },
              "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
              "type": "clozetext",
              "widget_type": "response"
            }
          ],
          "status": "published",
          "title": "item"
        }
      ],
      "metadata": {
        "author_version": "4.2.0",
        "content": "item",
        "created_by": "library(examiner)",
        "date_created": "<date>"
      }
    }

# wiseflow_export_items handles fill_in_multiple_numerics_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["definition", "questions", "status", "title"]
                }
              },
              "value": [
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "metadata", "reference", "type", "widget_type"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["is_math", "response_container", "response_containers", "stimulus", "template", "type", "ui_style", "validation"]
                            }
                          },
                          "value": [
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["template"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": [""]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": []
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Fill In Multiple Numerics<\/strong> question (fill_in_multiple_numerics_question).<\/p>\n<p>Note: This works for WISEflow, but not Canvas.<\/p>\n<p>The syntax for question text with placeholders is similar to that of <code>fill_in_multiple_blanks_question<\/code>, except placeholders are demarcated by double curly braces instead of square brackets.<\/p>\n<p>The syntax for answer options follows that of <code>numerical_question<\/code>.<\/p>\n<p><strong>Example:<\/strong><\/p>\n<p>What is the range of the <code>atan()<\/code> function?<\/p>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>{{response}} to {{response}}.<\/p>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["clozeformulaV2"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["floating-keyboard"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["rounding", "scoring_type", "valid_response"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["none"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["partialMatchV2"]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["score", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    },
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": [
                                            {
                                              "type": "list",
                                              "attributes": {
                                                "names": {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["method", "value"]
                                                }
                                              },
                                              "value": [
                                                {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["equivValue"]
                                                },
                                                {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["-1.5707963"]
                                                }
                                              ]
                                            }
                                          ]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": [
                                            {
                                              "type": "list",
                                              "attributes": {
                                                "names": {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["method", "value"]
                                                }
                                              },
                                              "value": [
                                                {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["equivValue"]
                                                },
                                                {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["1.5707963"]
                                                }
                                              ]
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Fill in maths"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["clozeformulaV2"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "content", "created_by", "date_created"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items handles short_answer_question

    {
      "data": [
        {
          "definition": {
            "template": "dynamic",
            "widgets": [
              {
                "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                "widget_type": "response"
              }
            ]
          },
          "questions": [
            {
              "data": {
                "stimulus": "<p>This is a <strong>Fill In the Blank<\/strong> question (short_answer_question).\nWhat colour is blood?<\/p>",
                "type": "shorttext",
                "validation": {
                  "allow_negative_scores": true,
                  "alt_responses": [
                    {
                      "score": 1,
                      "value": "Crimson"
                    }
                  ],
                  "scoring_type": "exactMatch",
                  "valid_response": {
                    "score": 1,
                    "value": "Red"
                  }
                }
              },
              "metadata": {
                "name": "Short text"
              },
              "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
              "type": "shorttext",
              "widget_type": "response"
            }
          ],
          "status": "published",
          "title": "item"
        }
      ],
      "metadata": {
        "author_version": "4.2.0",
        "content": "item",
        "created_by": "library(examiner)",
        "date_created": "<date>"
      }
    }

# wiseflow_export_items handles matching_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["definition", "questions", "status", "title"]
                }
              },
              "value": [
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "metadata", "reference", "type", "widget_type"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["possible_responses", "shuffle_options", "stimulus", "stimulus_list", "type", "validation"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["red", "blue", "white", "green", "violet"]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Matching<\/strong> question (matching_question).<\/p>\n<p>Please pick the traditional colour for each flower:<\/p>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["roses", "violets"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["association"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["allow_negative_scores", "rounding", "scoring_type", "valid_response"]
                                }
                              },
                              "value": [
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["none"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["partialMatchV2"]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["score", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["red", "blue"]
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Pair elements"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["association"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "content", "created_by", "date_created"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items handles essay_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["definition", "questions", "status", "title"]
                }
              },
              "value": [
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "metadata", "reference", "type", "widget_type"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["formatting_options", "stimulus", "submit_over_limit", "type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["bold", "italic", "underline", "unorderedList", "orderedList", "removeFormat", "superscript", "subscript", "indentIncrease", "indentDecrease", "table", "image", "characterMathMap", "undo", "redo"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is an <strong>Essay Question<\/strong> (essay_question).<\/p>\n<p>Write a small poem with some formatting.<\/p>\n<\/div>"]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["longtextV2"]
                            }
                          ]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Essay"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["longtextV2"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "content", "created_by", "date_created"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items handles file_upload_question

    {
      "data": [
        {
          "definition": {
            "template": "dynamic",
            "widgets": [
              {
                "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                "widget_type": "response"
              }
            ]
          },
          "questions": [
            {
              "data": {
                "allow_gif": true,
                "allow_jpg": true,
                "allow_pdf": true,
                "allow_png": true,
                "max_files": 1,
                "photo_capture": true,
                "stimulus": "<p>This is a <strong>File Upload Question<\/strong> (file_upload_question).<\/p>\n<p>Please upload a file of your own choosing.<\/p>\n<\/div>",
                "type": "fileupload",
                "validation": {
                  "max_score": 1
                }
              },
              "metadata": {
                "name": "File upload"
              },
              "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
              "type": "fileupload",
              "widget_type": "response"
            }
          ],
          "status": "published",
          "title": "item"
        }
      ],
      "metadata": {
        "author_version": "4.2.0",
        "content": "item",
        "created_by": "library(examiner)",
        "date_created": "<date>"
      }
    }

# wiseflow_export_items handles numerical_question

    [
      {
        "score": 1,
        "value": [
          [
            {
              "method": "equivValue",
              "value": "366"
            }
          ]
        ]
      },
      {
        "score": 1,
        "value": [
          [
            {
              "method": "equivValue",
              "options": {
                "tolerance": "\\range",
                "toleranceMaximum": 31,
                "toleranceMinimum": 28
              },
              "value": "29.5"
            }
          ]
        ]
      },
      {
        "score": 1,
        "value": [
          [
            {
              "method": "equivValue",
              "options": {
                "tolerance": "\\range",
                "toleranceMaximum": 294,
                "toleranceMinimum": 266
              },
              "value": "280"
            }
          ]
        ]
      },
      {
        "score": 1,
        "value": [
          [
            {
              "method": "equivValue",
              "options": {
                "tolerance": "\\range",
                "toleranceMaximum": 745000,
                "toleranceMinimum": 735000
              },
              "value": "740000"
            }
          ]
        ]
      }
    ]

---

    {
      "data": [
        {
          "definition": {
            "template": "dynamic",
            "widgets": [
              {
                "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                "widget_type": "response"
              }
            ]
          },
          "questions": [
            {
              "data": {
                "is_math": true,
                "response_container": {
                  "template": ""
                },
                "response_containers": [],
                "stimulus": "<p>An example of the Canvas question type called <strong>Numerical Answer<\/strong> question (numerical_question).<\/p>\n<p>It showcases the various ways of specifying numeric answers.<\/p>\n<p>How many days are there in a year, a month, a normal human gestation since last menstrual period, or since the beginning of year 1?<\/p>",
                "template": "{{response}}",
                "type": "clozeformulaV2",
                "ui_style": {
                  "type": "floating-keyboard"
                },
                "validation": {
                  "alt_responses": [
                    {
                      "score": 1,
                      "value": [
                        [
                          {
                            "method": "equivValue",
                            "value": "366"
                          }
                        ]
                      ]
                    },
                    {
                      "score": 1,
                      "value": [
                        [
                          {
                            "method": "equivValue",
                            "options": {
                              "tolerance": "\\range",
                              "toleranceMaximum": 31,
                              "toleranceMinimum": 28
                            },
                            "value": "29.5"
                          }
                        ]
                      ]
                    },
                    {
                      "score": 1,
                      "value": [
                        [
                          {
                            "method": "equivValue",
                            "options": {
                              "tolerance": "\\range",
                              "toleranceMaximum": 294,
                              "toleranceMinimum": 266
                            },
                            "value": "280"
                          }
                        ]
                      ]
                    },
                    {
                      "score": 1,
                      "value": [
                        [
                          {
                            "method": "equivValue",
                            "options": {
                              "tolerance": "\\range",
                              "toleranceMaximum": 745000,
                              "toleranceMinimum": 735000
                            },
                            "value": "740000"
                          }
                        ]
                      ]
                    }
                  ],
                  "scoring_type": "exactMatch",
                  "valid_response": {
                    "score": 1,
                    "value": [
                      [
                        {
                          "method": "equivValue",
                          "value": "365"
                        }
                      ]
                    ]
                  }
                }
              },
              "metadata": {
                "name": "Fill in maths"
              },
              "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
              "type": "clozeformulaV2",
              "widget_type": "response"
            }
          ],
          "status": "published",
          "title": "item"
        }
      ],
      "metadata": {
        "author_version": "4.2.0",
        "content": "item",
        "created_by": "library(examiner)",
        "date_created": "<date>"
      }
    }

# wiseflow_export_assignment handles numerical_question with one fixed answer

    {
      "data": [
        {
          "data": {
            "items": [
              {
                "definition": {
                  "template": "dynamic",
                  "widgets": [
                    {
                      "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                      "widget_type": "response"
                    }
                  ]
                },
                "questions": [
                  {
                    "data": {
                      "is_math": true,
                      "response_container": {
                        "template": ""
                      },
                      "response_containers": [],
                      "stimulus": "<p>The answer is 42.<\/p>",
                      "template": "{{response}}",
                      "type": "clozeformulaV2",
                      "ui_style": {
                        "type": "floating-keyboard"
                      },
                      "validation": {
                        "alt_responses": [],
                        "scoring_type": "exactMatch",
                        "valid_response": {
                          "score": 1,
                          "value": [
                            [
                              {
                                "method": "equivValue",
                                "value": "42"
                              }
                            ]
                          ]
                        }
                      }
                    },
                    "metadata": {
                      "name": "Fill in maths"
                    },
                    "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                    "type": "clozeformulaV2",
                    "widget_type": "response"
                  }
                ],
                "source": "",
                "status": "published",
                "title": "Item 1",
                "workflow": []
              }
            ],
            "rendering_type": "assess"
          },
          "description": "",
          "settings": {
            "information_page": true,
            "page_content": ""
          },
          "status": "published",
          "title": "Test numerical_question in WISEflow"
        }
      ],
      "metadata": {
        "author_version": "4.2.0",
        "content": "activity",
        "created_by": "library(examiner)",
        "date_created": "<date>"
      }
    }

# wiseflow_export_assignment handles numerical_question with two fixed answers

    {
      "data": [
        {
          "data": {
            "items": [
              {
                "definition": {
                  "template": "dynamic",
                  "widgets": [
                    {
                      "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                      "widget_type": "response"
                    }
                  ]
                },
                "questions": [
                  {
                    "data": {
                      "is_math": true,
                      "response_container": {
                        "template": ""
                      },
                      "response_containers": [],
                      "stimulus": "<p>The answer is 42 or 1024.<\/p>",
                      "template": "{{response}}",
                      "type": "clozeformulaV2",
                      "ui_style": {
                        "type": "floating-keyboard"
                      },
                      "validation": {
                        "alt_responses": [
                          {
                            "score": 1,
                            "value": [
                              [
                                {
                                  "method": "equivValue",
                                  "value": "1024"
                                }
                              ]
                            ]
                          }
                        ],
                        "scoring_type": "exactMatch",
                        "valid_response": {
                          "score": 1,
                          "value": [
                            [
                              {
                                "method": "equivValue",
                                "value": "42"
                              }
                            ]
                          ]
                        }
                      }
                    },
                    "metadata": {
                      "name": "Fill in maths"
                    },
                    "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                    "type": "clozeformulaV2",
                    "widget_type": "response"
                  }
                ],
                "source": "",
                "status": "published",
                "title": "Item 1",
                "workflow": []
              }
            ],
            "rendering_type": "assess"
          },
          "description": "",
          "settings": {
            "information_page": true,
            "page_content": ""
          },
          "status": "published",
          "title": "Test numerical_question in WISEflow"
        }
      ],
      "metadata": {
        "author_version": "4.2.0",
        "content": "activity",
        "created_by": "library(examiner)",
        "date_created": "<date>"
      }
    }

# wiseflow_export_assignment handles numerical_question with range_answer

    {
      "data": [
        {
          "data": {
            "items": [
              {
                "definition": {
                  "template": "dynamic",
                  "widgets": [
                    {
                      "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                      "widget_type": "response"
                    }
                  ]
                },
                "questions": [
                  {
                    "data": {
                      "is_math": true,
                      "response_container": {
                        "template": ""
                      },
                      "response_containers": [],
                      "stimulus": "<p>The answer is 28-31.<\/p>",
                      "template": "{{response}}",
                      "type": "clozeformulaV2",
                      "ui_style": {
                        "type": "floating-keyboard"
                      },
                      "validation": {
                        "alt_responses": [],
                        "scoring_type": "exactMatch",
                        "valid_response": {
                          "score": 1,
                          "value": [
                            [
                              {
                                "method": "equivValue",
                                "options": {
                                  "tolerance": "\\range",
                                  "toleranceMaximum": 31,
                                  "toleranceMinimum": 28
                                },
                                "value": "29.5"
                              }
                            ]
                          ]
                        }
                      }
                    },
                    "metadata": {
                      "name": "Fill in maths"
                    },
                    "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                    "type": "clozeformulaV2",
                    "widget_type": "response"
                  }
                ],
                "source": "",
                "status": "published",
                "title": "Item 1",
                "workflow": []
              }
            ],
            "rendering_type": "assess"
          },
          "description": "",
          "settings": {
            "information_page": true,
            "page_content": ""
          },
          "status": "published",
          "title": "Test numerical_question in WISEflow"
        }
      ],
      "metadata": {
        "author_version": "4.2.0",
        "content": "activity",
        "created_by": "library(examiner)",
        "date_created": "<date>"
      }
    }

# wiseflow_export_assignment handles numerical_question with margin

    {
      "data": [
        {
          "data": {
            "items": [
              {
                "definition": {
                  "template": "dynamic",
                  "widgets": [
                    {
                      "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                      "widget_type": "response"
                    }
                  ]
                },
                "questions": [
                  {
                    "data": {
                      "is_math": true,
                      "response_container": {
                        "template": ""
                      },
                      "response_containers": [],
                      "stimulus": "<p>The answer is <span class=\"math inline\">\\(280 \\pm 14\\)<\/span>.<\/p>",
                      "template": "{{response}}",
                      "type": "clozeformulaV2",
                      "ui_style": {
                        "type": "floating-keyboard"
                      },
                      "validation": {
                        "alt_responses": [],
                        "scoring_type": "exactMatch",
                        "valid_response": {
                          "score": 1,
                          "value": [
                            [
                              {
                                "method": "equivValue",
                                "options": {
                                  "tolerance": "\\range",
                                  "toleranceMaximum": 294,
                                  "toleranceMinimum": 266
                                },
                                "value": "280"
                              }
                            ]
                          ]
                        }
                      }
                    },
                    "metadata": {
                      "name": "Fill in maths"
                    },
                    "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                    "type": "clozeformulaV2",
                    "widget_type": "response"
                  }
                ],
                "source": "",
                "status": "published",
                "title": "Item 1",
                "workflow": []
              }
            ],
            "rendering_type": "assess"
          },
          "description": "",
          "settings": {
            "information_page": true,
            "page_content": ""
          },
          "status": "published",
          "title": "Test numerical_question in WISEflow"
        }
      ],
      "metadata": {
        "author_version": "4.2.0",
        "content": "activity",
        "created_by": "library(examiner)",
        "date_created": "<date>"
      }
    }

# wiseflow_export_assignment handles numerical_question, precision, penalty

    {
      "data": [
        {
          "data": {
            "items": [
              {
                "definition": {
                  "template": "dynamic",
                  "widgets": [
                    {
                      "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                      "widget_type": "response"
                    }
                  ]
                },
                "questions": [
                  {
                    "data": {
                      "is_math": true,
                      "response_container": {
                        "template": ""
                      },
                      "response_containers": [],
                      "stimulus": "<p>The answer is pi to three significant digits.<\/p>",
                      "template": "{{response}}",
                      "type": "clozeformulaV2",
                      "ui_style": {
                        "type": "floating-keyboard"
                      },
                      "validation": {
                        "alt_responses": [],
                        "scoring_type": "exactMatch",
                        "valid_response": {
                          "score": 1,
                          "value": [
                            [
                              {
                                "method": "equivValue",
                                "options": {
                                  "tolerance": "\\range",
                                  "toleranceMaximum": 3.145,
                                  "toleranceMinimum": 3.135
                                },
                                "value": "3.14"
                              }
                            ]
                          ]
                        }
                      }
                    },
                    "metadata": {
                      "name": "Fill in maths"
                    },
                    "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                    "type": "clozeformulaV2",
                    "widget_type": "response"
                  }
                ],
                "source": "",
                "status": "published",
                "title": "Item 1",
                "workflow": []
              }
            ],
            "rendering_type": "assess"
          },
          "description": "",
          "settings": {
            "information_page": true,
            "page_content": ""
          },
          "status": "published",
          "title": "Test numerical_question in WISEflow"
        }
      ],
      "metadata": {
        "author_version": "4.2.0",
        "content": "activity",
        "created_by": "library(examiner)",
        "date_created": "<date>"
      }
    }

# wiseflow_export_assignment handles numerical_question

    list(data = list(list(data = list(items = list(list(definition = list(
        template = "dynamic", widgets = list(list(reference = "aaaaaaaa-aaaa-aaaa-aaaa-000000000001", 
            widget_type = "response"))), questions = list(list(data = list(
        is_math = TRUE, response_container = list(template = ""), 
        response_containers = list(), stimulus = "<p>An example of the Canvas question type called <strong>Numerical Answer</strong> question (numerical_question).</p>\n<p>It showcases the various ways of specifying numeric answers.</p>\n<p>How many days are there in a year, a month, a normal human gestation since last menstrual period, or since the beginning of year 1?</p>", 
        template = "{{response}}", type = "clozeformulaV2", ui_style = list(
            type = "floating-keyboard"), validation = list(alt_responses = list(
            list(score = 1L, value = list(list(list(method = "equivValue", 
                value = "366")))), list(score = 1L, value = list(
                list(list(method = "equivValue", options = list(tolerance = "\\range", 
                    toleranceMaximum = 31L, toleranceMinimum = 28L), 
                    value = "29.5")))), list(score = 1L, value = list(
                list(list(method = "equivValue", options = list(tolerance = "\\range", 
                    toleranceMaximum = 294L, toleranceMinimum = 266L), 
                    value = "280")))), list(score = 1L, value = list(
                list(list(method = "equivValue", options = list(tolerance = "\\range", 
                    toleranceMaximum = 745000L, toleranceMinimum = 735000L), 
                    value = "740000"))))), scoring_type = "exactMatch", 
            valid_response = list(score = 1L, value = list(list(list(
                method = "equivValue", value = "365")))))), metadata = list(
        name = "Fill in maths"), reference = "aaaaaaaa-aaaa-aaaa-aaaa-000000000001", 
        type = "clozeformulaV2", widget_type = "response")), source = "", 
        status = "published", title = "Item 1", workflow = list())), 
        rendering_type = "assess"), description = "", settings = list(
        information_page = TRUE, page_content = ""), status = "published", 
        title = "Test numerical_question")), metadata = list(author_version = "4.2.0", 
        content = "activity", created_by = "library(examiner)", date_created = "<date>"))

# wiseflow_export_items handles text_only_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["definition", "features", "questions", "status", "title"]
                }
              },
              "value": [
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["feature"]
                            }
                          ]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000002"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "reference", "type", "widget_type"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["content", "type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Text Only Question<\/strong> (text_only_question) for testing.<\/p>\n<\/div>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["sharedpassage"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["sharedpassage"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["feature"]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "metadata", "reference", "type", "widget_type"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["metadata", "options", "shuffle_options", "stimulus", "type", "ui_style", "validation"]
                            }
                          },
                          "value": [
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["distractor_rationale_response_level"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "NULL"
                                    },
                                    {
                                      "type": "NULL"
                                    },
                                    {
                                      "type": "NULL"
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Red<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["0"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>White<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["1"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Blue<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["2"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Multiple Choice<\/strong> question (multiple_choice_question).<\/p>\n<p>What is the colour of blood?<\/p>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["mcq"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["choice_label", "type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["upper-alpha"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["horizontal"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["scoring_type", "valid_response"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["exactMatch"]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["score", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    },
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["0"]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Multiple responses"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000002"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["mcq"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "content", "created_by", "date_created"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            }
          ]
        }
      ]
    }

# numerical_answer_canvas_to_wiseflow handles range_answer

    {
      "method": "equivValue",
      "value": "15",
      "options": {
        "tolerance": "\\range",
        "toleranceMinimum": 10,
        "toleranceMaximum": 20
      }
    }

# wiseflow_export_items handles fill_in_multiple_numerics_question (plain)

    {
      "data": [
        {
          "definition": {
            "template": "dynamic",
            "widgets": [
              {
                "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                "widget_type": "response"
              }
            ]
          },
          "questions": [
            {
              "data": {
                "is_math": true,
                "response_container": {
                  "template": ""
                },
                "response_containers": [],
                "stimulus": "<p>What is 2+2?<\/p>",
                "template": "<p>{{response}}<\/p>",
                "type": "clozeformulaV2",
                "ui_style": {
                  "type": "floating-keyboard"
                },
                "validation": {
                  "rounding": "none",
                  "scoring_type": "partialMatchV2",
                  "valid_response": {
                    "score": 1,
                    "value": [
                      [
                        {
                          "method": "equivValue",
                          "value": "4"
                        }
                      ]
                    ]
                  }
                }
              },
              "metadata": {
                "name": "Fill in maths"
              },
              "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
              "type": "clozeformulaV2",
              "widget_type": "response"
            }
          ],
          "status": "published",
          "title": "test"
        }
      ],
      "metadata": {
        "author_version": "4.2.0",
        "content": "item",
        "created_by": "library(examiner)",
        "date_created": "<date>"
      }
    }

# wiseflow_export_assignment handles fill_in_multiple_numerics_question (all features)

    {
      "data": [
        {
          "data": {
            "items": [
              {
                "definition": {
                  "template": "dynamic",
                  "widgets": [
                    {
                      "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                      "widget_type": "response"
                    }
                  ]
                },
                "questions": [
                  {
                    "data": {
                      "is_math": true,
                      "response_container": {
                        "template": ""
                      },
                      "response_containers": [],
                      "stimulus": "<p>Test numerical question features in fill_in_multiple_numerics_question:<\/p>",
                      "template": "<ul>\n<li>{{response}}<\/li>\n<li>{{response}}<\/li>\n<li>{{response}}<\/li>\n<li>{{response}}<\/li>\n<li>{{response}}<\/li>\n<\/ul>",
                      "type": "clozeformulaV2",
                      "ui_style": {
                        "type": "floating-keyboard"
                      },
                      "validation": {
                        "rounding": "none",
                        "scoring_type": "partialMatchV2",
                        "valid_response": {
                          "score": 1,
                          "value": [
                            [
                              {
                                "method": "equivValue",
                                "value": "365"
                              }
                            ],
                            [
                              {
                                "method": "equivValue",
                                "value": "366"
                              }
                            ],
                            [
                              {
                                "method": "equivValue",
                                "options": {
                                  "tolerance": "\\range",
                                  "toleranceMaximum": 31,
                                  "toleranceMinimum": 28
                                },
                                "value": "29.5"
                              }
                            ],
                            [
                              {
                                "method": "equivValue",
                                "options": {
                                  "tolerance": "\\range",
                                  "toleranceMaximum": 294,
                                  "toleranceMinimum": 266
                                },
                                "value": "280"
                              }
                            ],
                            [
                              {
                                "method": "equivValue",
                                "options": {
                                  "tolerance": "\\range",
                                  "toleranceMaximum": 745000,
                                  "toleranceMinimum": 735000
                                },
                                "value": "740000"
                              }
                            ]
                          ]
                        }
                      }
                    },
                    "metadata": {
                      "name": "Fill in maths"
                    },
                    "reference": "aaaaaaaa-aaaa-aaaa-aaaa-000000000001",
                    "type": "clozeformulaV2",
                    "widget_type": "response"
                  }
                ],
                "source": "",
                "status": "published",
                "title": "test",
                "workflow": []
              }
            ],
            "rendering_type": "assess"
          },
          "description": "",
          "settings": {
            "go_to_first_question": true,
            "information_page": false,
            "negative_scores": true,
            "no_backward_navigation": false,
            "reading_mode": false,
            "reading_time": 10,
            "shuffle_sections": false,
            "warning_on_section_change": true,
            "warning_time": 10
          },
          "status": "published",
          "title": "Test"
        }
      ],
      "metadata": {
        "author_version": "4.2.0",
        "content": "activity",
        "created_by": "library(examiner)",
        "date_created": "<date>"
      }
    }

