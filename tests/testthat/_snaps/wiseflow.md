# wiseflow_export_item handles multiple_choice_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "validation", "metadata", "shuffle_options", "options", "type", "ui_style"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Multiple Choice<\/strong> question (multiple_choice_question).<\/p>\n<p>What is the colour of blood?<\/p>"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["valid_response", "scoring_type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["value", "score"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["0"]
                                        }
                                      ]
                                    },
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    }
                                  ]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["exactMatch"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["distractor_rationale_response_level"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Blood is indeed red, arterial blood more so than venous blood.<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>No known organisms have white blood.\nWhite blood cells do exist, but the red blood cells dominate the colour of blood.<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>\n<a href=\"https://en.wikipedia.org/w/index.php?title=Nobility&amp;oldid=1220366260#cite_note-21\">Royal blood was considered \"blue\"<\/a> by the ancient Spaniards, since veins appear bluish through pale skin.\nHowever, closer inspection will reveal that blood is not actually blue.<\/div>"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Red<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["0"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>White<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["1"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Blue<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["2"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["mcq"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["choice_label", "type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["upper-alpha"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["horizontal"]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["mcq"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Multiple responses"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_item handles multiple_answers_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "validation", "metadata", "shuffle_options", "options", "type", "ui_style", "multiple_responses"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Multiple Answers<\/strong> question (multiple_answers_question).<\/p>\n<p>Which colours describe blood?\nCheck all that apply.<\/p>"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["valid_response", "allow_negative_scores", "scoring_type", "penalty", "min_score_if_attempted", "rounding"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["value", "score"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["0", "2"]
                                    },
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    }
                                  ]
                                },
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["partialMatchV2"]
                                },
                                {
                                  "type": "double",
                                  "attributes": {},
                                  "value": [1]
                                },
                                {
                                  "type": "double",
                                  "attributes": {},
                                  "value": [0]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["none"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["distractor_rationale_response_level"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": [""]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": [""]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Royalty are sometimes said to have \"blue blood\", so we'll accept this one too.<\/div>"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Red<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["0"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>White<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["1"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Blue<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["2"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["mcq"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["choice_label", "type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["upper-alpha"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["horizontal"]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["mcq"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Multiple responses"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_item handles multiple_dropdowns_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "validation", "possible_responses", "type", "template"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": [""]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["valid_response", "alt_responses", "allow_negative_scores", "scoring_type", "match_all_possible_responses", "rounding"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["score", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    },
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["up"]
                                        },
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["blue"]
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["score", "value"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "double",
                                          "attributes": {},
                                          "value": [1]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": [
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["up"]
                                            },
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["the limit"]
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["partialMatchV2"]
                                },
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["none"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["up", "down", "out"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["blue", "green", "the limit"]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["clozedropdown"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Multiple Dropdowns<\/strong> question (multiple_dropdowns_question).<\/p>\n<p>Please complete this line from \"Dear Prudence\" by the Beatles:<\/p>\n<p>The sun is {{response}}, the sky is {{response}}.<\/p>"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["clozedropdown"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Fill in text with drop-down"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_item handles fill_in_multiple_blanks_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "validation", "type", "template"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": [""]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["valid_response", "alt_responses", "allow_negative_scores", "scoring_type", "match_all_possible_responses", "rounding"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["score", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    },
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["up"]
                                        },
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["blue"]
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": []
                                },
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["partialMatchV2"]
                                },
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["none"]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["clozetext"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Fill In Multiple Blanks<\/strong> question (fill_in_multiple_blanks_question).<\/p>\n<p>Please complete this line from \"Dear Prudence\" by the Beatles:<\/p>\n<p>The sun is {{response}}, the sky is {{response}}.<\/p>"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["clozetext"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Fill in text"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_item handles short_answer_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "validation", "type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Fill In the Blank<\/strong> question (short_answer_question).\nWhat colour is blood?<\/p>"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["valid_response", "alt_responses", "allow_negative_scores", "scoring_type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["value", "score"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["Red"]
                                    },
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["score", "value"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "double",
                                          "attributes": {},
                                          "value": [1]
                                        },
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["Crimson"]
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["exactMatch"]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["shorttext"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["shorttext"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Short text"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_item handles matching_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "type", "stimulus_list", "possible_responses", "validation", "shuffle_options"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Matching<\/strong> question (matching_question).<\/p>\n<p>Please pick the traditional colour for each flower:<\/p>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["association"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["roses", "violets"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["red", "blue", "white", "green", "violet"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["allow_negative_scores", "scoring_type", "valid_response", "rounding"]
                                }
                              },
                              "value": [
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["partialMatchV2"]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["score", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["red", "blue"]
                                    }
                                  ]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["none"]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["association"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Pair elements"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_item handles essay_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "type", "submit_over_limit", "formatting_options"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is an <strong>Essay Question<\/strong> (essay_question).<\/p>\n<p>Write a small poem with some formatting.<\/p>\n<\/div>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["longtextV2"]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["bold", "italic", "underline", "unorderedList", "orderedList", "removeFormat", "superscript", "subscript", "indentIncrease", "indentDecrease", "table", "image", "characterMathMap", "undo", "redo"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["longtextV2"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Essay"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_item handles file_upload_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "type", "max_files", "validation", "allow_pdf", "allow_jpg", "allow_gif", "allow_png", "photo_capture"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>File Upload Question<\/strong> (file_upload_question).<\/p>\n<p>Please upload a file of your own choosing.<\/p>\n<\/div>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["fileupload"]
                            },
                            {
                              "type": "double",
                              "attributes": {},
                              "value": [1]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["max_score"]
                                }
                              },
                              "value": [
                                {
                                  "type": "double",
                                  "attributes": {},
                                  "value": [1]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["fileupload"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["File upload"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_item handles numerical_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "type", "template", "ui_style", "response_container", "response_containers", "is_math", "validation"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>An example of the Canvas question type called <strong>Numerical Answer<\/strong> question (numerical_question).<\/p>\n<p>It showcases the various ways of specifying numeric answers.<\/p>\n<p>How many days are there in a year, a month, a normal human gestation since last menstrual period, or since the beginning of year 1?<\/p>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["clozeformulaV2"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["{{response}}"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["floating-keyboard"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["template"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": [""]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": []
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["scoring_type", "valid_response"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["exactMatch"]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["value", "score"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": [
                                            {
                                              "type": "list",
                                              "attributes": {
                                                "names": {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["method", "value"]
                                                }
                                              },
                                              "value": [
                                                {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["equivValue"]
                                                },
                                                {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["365"]
                                                }
                                              ]
                                            },
                                            {
                                              "type": "list",
                                              "attributes": {
                                                "names": {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["method", "value"]
                                                }
                                              },
                                              "value": [
                                                {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["equivValue"]
                                                },
                                                {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["366"]
                                                }
                                              ]
                                            },
                                            {
                                              "type": "list",
                                              "attributes": {
                                                "names": {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["method", "value", "options"]
                                                }
                                              },
                                              "value": [
                                                {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["equivValue"]
                                                },
                                                {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["29.5"]
                                                },
                                                {
                                                  "type": "list",
                                                  "attributes": {
                                                    "names": {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["decimalPlaces", "setDecimalSeparator", "setThousandsSeparator", "tolerance", "toleranceMinimum", "toleranceMaximum"]
                                                    }
                                                  },
                                                  "value": [
                                                    {
                                                      "type": "double",
                                                      "attributes": {},
                                                      "value": [10]
                                                    },
                                                    {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["."]
                                                    },
                                                    {
                                                      "type": "list",
                                                      "attributes": {},
                                                      "value": []
                                                    },
                                                    {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["\\range"]
                                                    },
                                                    {
                                                      "type": "double",
                                                      "attributes": {},
                                                      "value": [28]
                                                    },
                                                    {
                                                      "type": "double",
                                                      "attributes": {},
                                                      "value": [31]
                                                    }
                                                  ]
                                                }
                                              ]
                                            },
                                            {
                                              "type": "list",
                                              "attributes": {
                                                "names": {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["method", "value"]
                                                }
                                              },
                                              "value": [
                                                {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["equivValue"]
                                                },
                                                {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["280"]
                                                }
                                              ]
                                            },
                                            {
                                              "type": "list",
                                              "attributes": {
                                                "names": {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["method", "value", "options"]
                                                }
                                              },
                                              "value": [
                                                {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["equivValue"]
                                                },
                                                {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["740000"]
                                                },
                                                {
                                                  "type": "list",
                                                  "attributes": {
                                                    "names": {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["decimalPlaces", "setDecimalSeparator", "setThousandsSeparator"]
                                                    }
                                                  },
                                                  "value": [
                                                    {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["2"]
                                                    },
                                                    {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["."]
                                                    },
                                                    {
                                                      "type": "list",
                                                      "attributes": {},
                                                      "value": []
                                                    }
                                                  ]
                                                }
                                              ]
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["clozeformulaV2"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Fill in maths"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_item handles text_only_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions", "features"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["feature"]
                            }
                          ]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000002"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "validation", "metadata", "shuffle_options", "options", "type", "ui_style"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Multiple Choice<\/strong> question (multiple_choice_question).<\/p>\n<p>What is the colour of blood?<\/p>"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["valid_response", "scoring_type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["value", "score"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["0"]
                                        }
                                      ]
                                    },
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    }
                                  ]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["exactMatch"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["distractor_rationale_response_level"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "NULL"
                                    },
                                    {
                                      "type": "NULL"
                                    },
                                    {
                                      "type": "NULL"
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Red<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["0"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>White<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["1"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Blue<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["2"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["mcq"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["choice_label", "type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["upper-alpha"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["horizontal"]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000002"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["mcq"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Multiple responses"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "type", "widget_type", "reference"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["type", "content"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["sharedpassage"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Text Only Question<\/strong> (text_only_question).<\/p>\n<p>It is purely informational and not really a question.\nYou don't have to make any kind of response.<\/p>\n<p>Here's a list of all the question types in Canvas.\n(Note that this is \"Old\" Quizzes, not \"New Quizzes\" aka Quizzes.Next.\nThe latter has no API, results export or SpeedGrader yet.)<\/p>\n<table><colgroup><col width=\"50%\"/><col width=\"50%\"/><\/colgroup><thead><tr class=\"header\"><th>Label and <code>question_type<\/code><\/th>\n<th>Description<\/th>\n<\/tr><\/thead><tbody><tr class=\"odd\"><td>Multiple Choice <br/><code>multiple_choice_question<\/code><\/td>\n<td>Enter your question and multiple answers, then select the one correct answer.<\/td>\n<\/tr><tr class=\"even\"><td>Multiple Answers <br/><code>multiple_answers_question<\/code><\/td>\n<td>This question will show a checkbox next to each answer, and the student must select ALL the answers you mark as correct.<\/td>\n<\/tr><tr class=\"odd\"><td>Fill In the Blank <br/><code>short_answer_question<\/code><\/td>\n<td>Enter your question text, then define all possible correct answers for the blank. Students will see the question followed by a small text box to type their answer.<\/td>\n<\/tr><tr class=\"even\"><td>Fill In Multiple Blanks <br/><code>fill_in_multiple_blanks_question<\/code><\/td>\n<td>Enter your question, specifying where each blank should go. Then define the possible correct answer for each blank. Students must type correct answers into text boxes at each blank. [...] In the box below, every place you want to show an answer box, type a reference word (no spaces) surrounded by brackets (i.e. \"Roses are [color1], violets are [color2]\")<\/td>\n<\/tr><tr class=\"odd\"><td>Multiple Dropdowns <br/><code>multiple_dropdowns_question<\/code><\/td>\n<td>Enter your question, specifying where each dropdown should go. Then define possible answers for each dropdown, with one correct answer per dropdown. [...] In the box below, every place you want to show an answer box, type a reference word (no spaces) surrounded by brackets (i.e. \"Roses are [color1], violets are [color2]\")<\/td>\n<\/tr><tr class=\"even\"><td>Matching <br/><code>matching_question<\/code><\/td>\n<td>Build pairs of matching values. Students will see values on the left and have to select the matching value on the right from a dropdown. Multiple rows can have the same answer, and you can add additional distractors to the right side.<\/td>\n<\/tr><tr class=\"odd\"><td>Numerical Answer <br/><code>numerical_question<\/code><\/td>\n<td>Define the correct answer as any number within a range, or a number plus or minus some error margin. Student will be given an empty text box to type their numerical answer.<\/td>\n<\/tr><tr class=\"even\"><td>Essay Question <br/><code>essay_question<\/code><\/td>\n<td>Students will be given a text field to compose their answer.<\/td>\n<\/tr><tr class=\"odd\"><td>File Upload Question <br/><code>file_upload_question<\/code><\/td>\n<td>Students will be able to upload a file for their answer.<\/td>\n<\/tr><tr class=\"even\"><td>Text (no question) <br/><code>text_only_question<\/code><\/td>\n<td>This \"question\" will not be scored, but can be useful for introducing a set of related questions.<\/td>\n<\/tr><tr class=\"odd\"><td>True/False <br/><code>true_false_question<\/code><\/td>\n<td>(Not included, easily replaced by multiple_choice_question.)<\/td>\n<\/tr><tr class=\"even\"><td>Formula Question <br/><code>calculated_question<\/code><\/td>\n<td>(Randomized numerical exercises. Not included yet, can be done with R.)<\/td>\n<\/tr><\/tbody><\/table><\/div>"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["sharedpassage"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["feature"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

