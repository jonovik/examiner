# wiseflow_export_items handles multiple_choice_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "validation", "metadata", "shuffle_options", "options", "type", "ui_style"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Multiple Choice<\/strong> question (multiple_choice_question).<\/p>\n<p>What is the colour of blood?<\/p>"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["valid_response", "scoring_type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["value", "score"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["0"]
                                        }
                                      ]
                                    },
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    }
                                  ]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["exactMatch"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["distractor_rationale_response_level"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Blood is indeed red, arterial blood more so than venous blood.<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>No known organisms have white blood.\nWhite blood cells do exist, but the red blood cells dominate the colour of blood.<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>\n<a href=\"https://en.wikipedia.org/w/index.php?title=Nobility&amp;oldid=1220366260#cite_note-21\">Royal blood was considered \"blue\"<\/a> by the ancient Spaniards, since veins appear bluish through pale skin.\nHowever, closer inspection will reveal that blood is not actually blue.<\/div>"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Red<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["0"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>White<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["1"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Blue<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["2"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["mcq"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["choice_label", "type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["upper-alpha"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["horizontal"]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["mcq"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Multiple responses"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items handles multiple_choice_question with abstain=TRUE

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "validation", "metadata", "shuffle_options", "options", "type", "ui_style"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Multiple Choice<\/strong> question (multiple_choice_question).<\/p>\n<p>What is the colour of blood?<\/p>"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["valid_response", "allow_negative_scores", "penalty", "alt_responses", "scoring_type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["value", "score"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["0"]
                                        }
                                      ]
                                    },
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    }
                                  ]
                                },
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "double",
                                  "attributes": {},
                                  "value": [0.5]
                                },
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["score", "value"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "double",
                                          "attributes": {},
                                          "value": [0]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": [
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["3"]
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["exactMatch"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["distractor_rationale_response_level"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "NULL"
                                    },
                                    {
                                      "type": "NULL"
                                    },
                                    {
                                      "type": "NULL"
                                    },
                                    {
                                      "type": "NULL"
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Red<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["0"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>White<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["1"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Blue<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["2"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>I wish to leave this question unanswered<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["3"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["mcq"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["choice_label", "type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["upper-alpha"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["horizontal"]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["mcq"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Multiple responses"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items allows penalty=TRUE as synonym for abstain=TRUE

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "validation", "metadata", "shuffle_options", "options", "type", "ui_style"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Multiple Choice<\/strong> question (multiple_choice_question).<\/p>\n<p>What is the colour of blood?<\/p>"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["valid_response", "allow_negative_scores", "penalty", "alt_responses", "scoring_type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["value", "score"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["0"]
                                        }
                                      ]
                                    },
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    }
                                  ]
                                },
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "double",
                                  "attributes": {},
                                  "value": [0.5]
                                },
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["score", "value"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "double",
                                          "attributes": {},
                                          "value": [0]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": [
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["3"]
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["exactMatch"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["distractor_rationale_response_level"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Blood is indeed red, arterial blood more so than venous blood.<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>No known organisms have white blood.\nWhite blood cells do exist, but the red blood cells dominate the colour of blood.<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>\n<a href=\"https://en.wikipedia.org/w/index.php?title=Nobility&amp;oldid=1220366260#cite_note-21\">Royal blood was considered \"blue\"<\/a> by the ancient Spaniards, since veins appear bluish through pale skin.\nHowever, closer inspection will reveal that blood is not actually blue.<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": [null]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Red<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["0"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>White<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["1"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Blue<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["2"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>I wish to leave this question unanswered<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["3"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["mcq"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["choice_label", "type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["upper-alpha"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["horizontal"]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["mcq"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Multiple responses"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items handles multiple_answers_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "validation", "metadata", "shuffle_options", "options", "type", "ui_style", "multiple_responses"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Multiple Answers<\/strong> question (multiple_answers_question).<\/p>\n<p>Which colours describe blood?\nCheck all that apply.<\/p>"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["valid_response", "allow_negative_scores", "scoring_type", "penalty", "min_score_if_attempted", "rounding"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["value", "score"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["0", "2"]
                                    },
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    }
                                  ]
                                },
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["partialMatchV2"]
                                },
                                {
                                  "type": "double",
                                  "attributes": {},
                                  "value": [1]
                                },
                                {
                                  "type": "double",
                                  "attributes": {},
                                  "value": [0]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["none"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["distractor_rationale_response_level"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": [""]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": [""]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Royalty are sometimes said to have \"blue blood\", so we'll accept this one too.<\/div>"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Red<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["0"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>White<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["1"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Blue<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["2"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["mcq"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["choice_label", "type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["upper-alpha"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["horizontal"]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["mcq"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Multiple responses"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items handles multiple_dropdowns_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "validation", "possible_responses", "type", "template"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": [""]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["valid_response", "alt_responses", "allow_negative_scores", "scoring_type", "match_all_possible_responses", "rounding"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["score", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    },
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["up"]
                                        },
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["blue"]
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["score", "value"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "double",
                                          "attributes": {},
                                          "value": [1]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": [
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["up"]
                                            },
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["the limit"]
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["partialMatchV2"]
                                },
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["none"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["up", "down", "out"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["blue", "green", "the limit"]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["clozedropdown"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Multiple Dropdowns<\/strong> question (multiple_dropdowns_question).<\/p>\n<p>Please complete this line from \"Dear Prudence\" by the Beatles:<\/p>\n<p>The sun is {{response}}, the sky is {{response}}.<\/p>"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["clozedropdown"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Fill in text with drop-down"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items handles fill_in_multiple_blanks_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "validation", "type", "template"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": [""]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["valid_response", "alt_responses", "allow_negative_scores", "scoring_type", "match_all_possible_responses", "rounding"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["score", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    },
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["up"]
                                        },
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["blue"]
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": []
                                },
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["partialMatchV2"]
                                },
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["none"]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["clozetext"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Fill In Multiple Blanks<\/strong> question (fill_in_multiple_blanks_question).<\/p>\n<p>Please complete this line from \"Dear Prudence\" by the Beatles:<\/p>\n<p>The sun is {{response}}, the sky is {{response}}.<\/p>"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["clozetext"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Fill in text"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items handles short_answer_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "validation", "type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Fill In the Blank<\/strong> question (short_answer_question).\nWhat colour is blood?<\/p>"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["valid_response", "alt_responses", "allow_negative_scores", "scoring_type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["value", "score"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["Red"]
                                    },
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["score", "value"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "double",
                                          "attributes": {},
                                          "value": [1]
                                        },
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["Crimson"]
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["exactMatch"]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["shorttext"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["shorttext"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Short text"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items handles matching_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "type", "stimulus_list", "possible_responses", "validation", "shuffle_options"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Matching<\/strong> question (matching_question).<\/p>\n<p>Please pick the traditional colour for each flower:<\/p>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["association"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["roses", "violets"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["red", "blue", "white", "green", "violet"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["allow_negative_scores", "scoring_type", "valid_response", "rounding"]
                                }
                              },
                              "value": [
                                {
                                  "type": "logical",
                                  "attributes": {},
                                  "value": [true]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["partialMatchV2"]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["score", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["red", "blue"]
                                    }
                                  ]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["none"]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["association"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Pair elements"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items handles essay_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "type", "submit_over_limit", "formatting_options"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is an <strong>Essay Question<\/strong> (essay_question).<\/p>\n<p>Write a small poem with some formatting.<\/p>\n<\/div>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["longtextV2"]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["bold", "italic", "underline", "unorderedList", "orderedList", "removeFormat", "superscript", "subscript", "indentIncrease", "indentDecrease", "table", "image", "characterMathMap", "undo", "redo"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["longtextV2"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Essay"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items handles file_upload_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "type", "max_files", "validation", "allow_pdf", "allow_jpg", "allow_gif", "allow_png", "photo_capture"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>File Upload Question<\/strong> (file_upload_question).<\/p>\n<p>Please upload a file of your own choosing.<\/p>\n<\/div>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["fileupload"]
                            },
                            {
                              "type": "double",
                              "attributes": {},
                              "value": [1]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["max_score"]
                                }
                              },
                              "value": [
                                {
                                  "type": "double",
                                  "attributes": {},
                                  "value": [1]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["fileupload"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["File upload"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_items handles numerical_question

    [
      {
        "value": [
          [
            {
              "method": "equivValue",
              "value": "366"
            }
          ]
        ],
        "score": 1
      },
      {
        "value": [
          [
            {
              "method": "equivValue",
              "value": "29.5",
              "options": {
                "tolerance": "\\range",
                "toleranceMinimum": 28,
                "toleranceMaximum": 31
              }
            }
          ]
        ],
        "score": 1
      },
      {
        "value": [
          [
            {
              "method": "equivValue",
              "value": "280",
              "options": {
                "tolerance": "\\range",
                "toleranceMinimum": 266,
                "toleranceMaximum": 294
              }
            }
          ]
        ],
        "score": 1
      },
      {
        "value": [
          [
            {
              "method": "equivValue",
              "value": "740000",
              "options": {
                "decimalPlaces": "2",
                "setDecimalSeparator": ".",
                "setThousandsSeparator": []
              }
            }
          ]
        ],
        "score": 1
      }
    ]

---

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "type", "template", "ui_style", "response_container", "response_containers", "is_math", "validation"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>An example of the Canvas question type called <strong>Numerical Answer<\/strong> question (numerical_question).<\/p>\n<p>It showcases the various ways of specifying numeric answers.<\/p>\n<p>How many days are there in a year, a month, a normal human gestation since last menstrual period, or since the beginning of year 1?<\/p>"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["clozeformulaV2"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["{{response}}"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["floating-keyboard"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["template"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": [""]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": []
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["scoring_type", "valid_response", "alt_responses"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["exactMatch"]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["value", "score"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": [
                                            {
                                              "type": "list",
                                              "attributes": {
                                                "names": {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["method", "value"]
                                                }
                                              },
                                              "value": [
                                                {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["equivValue"]
                                                },
                                                {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["365"]
                                                }
                                              ]
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["value", "score"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": [
                                            {
                                              "type": "list",
                                              "attributes": {},
                                              "value": [
                                                {
                                                  "type": "list",
                                                  "attributes": {
                                                    "names": {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["method", "value"]
                                                    }
                                                  },
                                                  "value": [
                                                    {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["equivValue"]
                                                    },
                                                    {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["366"]
                                                    }
                                                  ]
                                                }
                                              ]
                                            }
                                          ]
                                        },
                                        {
                                          "type": "double",
                                          "attributes": {},
                                          "value": [1]
                                        }
                                      ]
                                    },
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["value", "score"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": [
                                            {
                                              "type": "list",
                                              "attributes": {},
                                              "value": [
                                                {
                                                  "type": "list",
                                                  "attributes": {
                                                    "names": {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["method", "value", "options"]
                                                    }
                                                  },
                                                  "value": [
                                                    {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["equivValue"]
                                                    },
                                                    {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["29.5"]
                                                    },
                                                    {
                                                      "type": "list",
                                                      "attributes": {
                                                        "names": {
                                                          "type": "character",
                                                          "attributes": {},
                                                          "value": ["tolerance", "toleranceMinimum", "toleranceMaximum"]
                                                        }
                                                      },
                                                      "value": [
                                                        {
                                                          "type": "character",
                                                          "attributes": {},
                                                          "value": ["\\range"]
                                                        },
                                                        {
                                                          "type": "double",
                                                          "attributes": {},
                                                          "value": [28]
                                                        },
                                                        {
                                                          "type": "double",
                                                          "attributes": {},
                                                          "value": [31]
                                                        }
                                                      ]
                                                    }
                                                  ]
                                                }
                                              ]
                                            }
                                          ]
                                        },
                                        {
                                          "type": "double",
                                          "attributes": {},
                                          "value": [1]
                                        }
                                      ]
                                    },
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["value", "score"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": [
                                            {
                                              "type": "list",
                                              "attributes": {},
                                              "value": [
                                                {
                                                  "type": "list",
                                                  "attributes": {
                                                    "names": {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["method", "value", "options"]
                                                    }
                                                  },
                                                  "value": [
                                                    {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["equivValue"]
                                                    },
                                                    {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["280"]
                                                    },
                                                    {
                                                      "type": "list",
                                                      "attributes": {
                                                        "names": {
                                                          "type": "character",
                                                          "attributes": {},
                                                          "value": ["tolerance", "toleranceMinimum", "toleranceMaximum"]
                                                        }
                                                      },
                                                      "value": [
                                                        {
                                                          "type": "character",
                                                          "attributes": {},
                                                          "value": ["\\range"]
                                                        },
                                                        {
                                                          "type": "double",
                                                          "attributes": {},
                                                          "value": [266]
                                                        },
                                                        {
                                                          "type": "double",
                                                          "attributes": {},
                                                          "value": [294]
                                                        }
                                                      ]
                                                    }
                                                  ]
                                                }
                                              ]
                                            }
                                          ]
                                        },
                                        {
                                          "type": "double",
                                          "attributes": {},
                                          "value": [1]
                                        }
                                      ]
                                    },
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["value", "score"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": [
                                            {
                                              "type": "list",
                                              "attributes": {},
                                              "value": [
                                                {
                                                  "type": "list",
                                                  "attributes": {
                                                    "names": {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["method", "value", "options"]
                                                    }
                                                  },
                                                  "value": [
                                                    {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["equivValue"]
                                                    },
                                                    {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["740000"]
                                                    },
                                                    {
                                                      "type": "list",
                                                      "attributes": {
                                                        "names": {
                                                          "type": "character",
                                                          "attributes": {},
                                                          "value": ["decimalPlaces", "setDecimalSeparator", "setThousandsSeparator"]
                                                        }
                                                      },
                                                      "value": [
                                                        {
                                                          "type": "character",
                                                          "attributes": {},
                                                          "value": ["2"]
                                                        },
                                                        {
                                                          "type": "character",
                                                          "attributes": {},
                                                          "value": ["."]
                                                        },
                                                        {
                                                          "type": "list",
                                                          "attributes": {},
                                                          "value": []
                                                        }
                                                      ]
                                                    }
                                                  ]
                                                }
                                              ]
                                            }
                                          ]
                                        },
                                        {
                                          "type": "double",
                                          "attributes": {},
                                          "value": [1]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["clozeformulaV2"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Fill in maths"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# wiseflow_export_assignment handles numerical_question with one fixed answer

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["data", "description", "status", "title", "settings"]
                }
              },
              "value": [
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["items", "rendering_type"]
                    }
                  },
                  "value": [
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["title", "status", "definition", "questions", "source", "workflow"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Item 1"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["published"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["template", "widgets"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["dynamic"]
                                },
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["reference", "widget_type"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                                        },
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["response"]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["data", "widget_type", "reference", "type", "metadata"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["stimulus", "type", "template", "ui_style", "response_container", "response_containers", "is_math", "validation"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["<p>The answer is 42.<\/p>"]
                                        },
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["clozeformulaV2"]
                                        },
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["{{response}}"]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {
                                            "names": {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["type"]
                                            }
                                          },
                                          "value": [
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["floating-keyboard"]
                                            }
                                          ]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {
                                            "names": {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["template"]
                                            }
                                          },
                                          "value": [
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": [""]
                                            }
                                          ]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": []
                                        },
                                        {
                                          "type": "logical",
                                          "attributes": {},
                                          "value": [true]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {
                                            "names": {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["scoring_type", "valid_response", "alt_responses"]
                                            }
                                          },
                                          "value": [
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["exactMatch"]
                                            },
                                            {
                                              "type": "list",
                                              "attributes": {
                                                "names": {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["value", "score"]
                                                }
                                              },
                                              "value": [
                                                {
                                                  "type": "list",
                                                  "attributes": {},
                                                  "value": [
                                                    {
                                                      "type": "list",
                                                      "attributes": {},
                                                      "value": [
                                                        {
                                                          "type": "list",
                                                          "attributes": {
                                                            "names": {
                                                              "type": "character",
                                                              "attributes": {},
                                                              "value": ["method", "value"]
                                                            }
                                                          },
                                                          "value": [
                                                            {
                                                              "type": "character",
                                                              "attributes": {},
                                                              "value": ["equivValue"]
                                                            },
                                                            {
                                                              "type": "character",
                                                              "attributes": {},
                                                              "value": ["42"]
                                                            }
                                                          ]
                                                        }
                                                      ]
                                                    }
                                                  ]
                                                },
                                                {
                                                  "type": "integer",
                                                  "attributes": {},
                                                  "value": [1]
                                                }
                                              ]
                                            },
                                            {
                                              "type": "list",
                                              "attributes": {},
                                              "value": []
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["response"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["clozeformulaV2"]
                                    },
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["name"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["Fill in maths"]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": [""]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": []
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["assess"]
                    }
                  ]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": [""]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["Test numerical_question in WISEflow"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["information_page", "page_content"]
                    }
                  },
                  "value": [
                    {
                      "type": "logical",
                      "attributes": {},
                      "value": [true]
                    },
                    {
                      "type": "character",
                      "attributes": {},
                      "value": [""]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["activity"]
            }
          ]
        }
      ]
    }

# wiseflow_export_assignment handles numerical_question with two fixed answers

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["data", "description", "status", "title", "settings"]
                }
              },
              "value": [
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["items", "rendering_type"]
                    }
                  },
                  "value": [
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["title", "status", "definition", "questions", "source", "workflow"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Item 1"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["published"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["template", "widgets"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["dynamic"]
                                },
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["reference", "widget_type"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                                        },
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["response"]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["data", "widget_type", "reference", "type", "metadata"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["stimulus", "type", "template", "ui_style", "response_container", "response_containers", "is_math", "validation"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["<p>The answer is 42 or 1024.<\/p>"]
                                        },
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["clozeformulaV2"]
                                        },
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["{{response}}"]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {
                                            "names": {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["type"]
                                            }
                                          },
                                          "value": [
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["floating-keyboard"]
                                            }
                                          ]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {
                                            "names": {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["template"]
                                            }
                                          },
                                          "value": [
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": [""]
                                            }
                                          ]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": []
                                        },
                                        {
                                          "type": "logical",
                                          "attributes": {},
                                          "value": [true]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {
                                            "names": {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["scoring_type", "valid_response", "alt_responses"]
                                            }
                                          },
                                          "value": [
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["exactMatch"]
                                            },
                                            {
                                              "type": "list",
                                              "attributes": {
                                                "names": {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["value", "score"]
                                                }
                                              },
                                              "value": [
                                                {
                                                  "type": "list",
                                                  "attributes": {},
                                                  "value": [
                                                    {
                                                      "type": "list",
                                                      "attributes": {},
                                                      "value": [
                                                        {
                                                          "type": "list",
                                                          "attributes": {
                                                            "names": {
                                                              "type": "character",
                                                              "attributes": {},
                                                              "value": ["method", "value"]
                                                            }
                                                          },
                                                          "value": [
                                                            {
                                                              "type": "character",
                                                              "attributes": {},
                                                              "value": ["equivValue"]
                                                            },
                                                            {
                                                              "type": "character",
                                                              "attributes": {},
                                                              "value": ["42"]
                                                            }
                                                          ]
                                                        }
                                                      ]
                                                    }
                                                  ]
                                                },
                                                {
                                                  "type": "integer",
                                                  "attributes": {},
                                                  "value": [1]
                                                }
                                              ]
                                            },
                                            {
                                              "type": "list",
                                              "attributes": {},
                                              "value": [
                                                {
                                                  "type": "list",
                                                  "attributes": {
                                                    "names": {
                                                      "type": "character",
                                                      "attributes": {},
                                                      "value": ["value", "score"]
                                                    }
                                                  },
                                                  "value": [
                                                    {
                                                      "type": "list",
                                                      "attributes": {},
                                                      "value": [
                                                        {
                                                          "type": "list",
                                                          "attributes": {},
                                                          "value": [
                                                            {
                                                              "type": "list",
                                                              "attributes": {
                                                                "names": {
                                                                  "type": "character",
                                                                  "attributes": {},
                                                                  "value": ["method", "value"]
                                                                }
                                                              },
                                                              "value": [
                                                                {
                                                                  "type": "character",
                                                                  "attributes": {},
                                                                  "value": ["equivValue"]
                                                                },
                                                                {
                                                                  "type": "character",
                                                                  "attributes": {},
                                                                  "value": ["1024"]
                                                                }
                                                              ]
                                                            }
                                                          ]
                                                        }
                                                      ]
                                                    },
                                                    {
                                                      "type": "integer",
                                                      "attributes": {},
                                                      "value": [1]
                                                    }
                                                  ]
                                                }
                                              ]
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["response"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["clozeformulaV2"]
                                    },
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["name"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["Fill in maths"]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": [""]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": []
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["assess"]
                    }
                  ]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": [""]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["Test numerical_question in WISEflow"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["information_page", "page_content"]
                    }
                  },
                  "value": [
                    {
                      "type": "logical",
                      "attributes": {},
                      "value": [true]
                    },
                    {
                      "type": "character",
                      "attributes": {},
                      "value": [""]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["activity"]
            }
          ]
        }
      ]
    }

# wiseflow_export_assignment handles numerical_question with range_answer

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["data", "description", "status", "title", "settings"]
                }
              },
              "value": [
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["items", "rendering_type"]
                    }
                  },
                  "value": [
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["title", "status", "definition", "questions", "source", "workflow"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Item 1"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["published"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["template", "widgets"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["dynamic"]
                                },
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["reference", "widget_type"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                                        },
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["response"]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["data", "widget_type", "reference", "type", "metadata"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["stimulus", "type", "template", "ui_style", "response_container", "response_containers", "is_math", "validation"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["<p>The answer is 28-31.<\/p>"]
                                        },
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["clozeformulaV2"]
                                        },
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["{{response}}"]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {
                                            "names": {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["type"]
                                            }
                                          },
                                          "value": [
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["floating-keyboard"]
                                            }
                                          ]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {
                                            "names": {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["template"]
                                            }
                                          },
                                          "value": [
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": [""]
                                            }
                                          ]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {},
                                          "value": []
                                        },
                                        {
                                          "type": "logical",
                                          "attributes": {},
                                          "value": [true]
                                        },
                                        {
                                          "type": "list",
                                          "attributes": {
                                            "names": {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["scoring_type", "valid_response", "alt_responses"]
                                            }
                                          },
                                          "value": [
                                            {
                                              "type": "character",
                                              "attributes": {},
                                              "value": ["exactMatch"]
                                            },
                                            {
                                              "type": "list",
                                              "attributes": {
                                                "names": {
                                                  "type": "character",
                                                  "attributes": {},
                                                  "value": ["value", "score"]
                                                }
                                              },
                                              "value": [
                                                {
                                                  "type": "list",
                                                  "attributes": {},
                                                  "value": [
                                                    {
                                                      "type": "list",
                                                      "attributes": {},
                                                      "value": [
                                                        {
                                                          "type": "list",
                                                          "attributes": {
                                                            "names": {
                                                              "type": "character",
                                                              "attributes": {},
                                                              "value": ["method", "value", "options"]
                                                            }
                                                          },
                                                          "value": [
                                                            {
                                                              "type": "character",
                                                              "attributes": {},
                                                              "value": ["equivValue"]
                                                            },
                                                            {
                                                              "type": "character",
                                                              "attributes": {},
                                                              "value": ["29.5"]
                                                            },
                                                            {
                                                              "type": "list",
                                                              "attributes": {
                                                                "names": {
                                                                  "type": "character",
                                                                  "attributes": {},
                                                                  "value": ["tolerance", "toleranceMinimum", "toleranceMaximum"]
                                                                }
                                                              },
                                                              "value": [
                                                                {
                                                                  "type": "character",
                                                                  "attributes": {},
                                                                  "value": ["\\range"]
                                                                },
                                                                {
                                                                  "type": "integer",
                                                                  "attributes": {},
                                                                  "value": [28]
                                                                },
                                                                {
                                                                  "type": "integer",
                                                                  "attributes": {},
                                                                  "value": [31]
                                                                }
                                                              ]
                                                            }
                                                          ]
                                                        }
                                                      ]
                                                    }
                                                  ]
                                                },
                                                {
                                                  "type": "integer",
                                                  "attributes": {},
                                                  "value": [1]
                                                }
                                              ]
                                            },
                                            {
                                              "type": "list",
                                              "attributes": {},
                                              "value": []
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["response"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["clozeformulaV2"]
                                    },
                                    {
                                      "type": "list",
                                      "attributes": {
                                        "names": {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["name"]
                                        }
                                      },
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["Fill in maths"]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": [""]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": []
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["assess"]
                    }
                  ]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": [""]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["Test numerical_question in WISEflow"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["information_page", "page_content"]
                    }
                  },
                  "value": [
                    {
                      "type": "logical",
                      "attributes": {},
                      "value": [true]
                    },
                    {
                      "type": "character",
                      "attributes": {},
                      "value": [""]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["activity"]
            }
          ]
        }
      ]
    }

# wiseflow_export_assignment handles numerical_question

    list(data = list(list(data = list(items = list(list(title = "Item 1", 
        status = "published", definition = list(template = "dynamic", 
            widgets = list(list(reference = "aaaaaaaa-aaaa-aaaa-aaaa-000000000001", 
                widget_type = "response"))), questions = list(list(
            data = list(stimulus = "<p>An example of the Canvas question type called <strong>Numerical Answer</strong> question (numerical_question).</p>\n<p>It showcases the various ways of specifying numeric answers.</p>\n<p>How many days are there in a year, a month, a normal human gestation since last menstrual period, or since the beginning of year 1?</p>", 
                type = "clozeformulaV2", template = "{{response}}", 
                ui_style = list(type = "floating-keyboard"), response_container = list(
                    template = ""), response_containers = list(), 
                is_math = TRUE, validation = list(scoring_type = "exactMatch", 
                    valid_response = list(value = list(list(list(
                      method = "equivValue", value = "365"))), score = 1L), 
                    alt_responses = list(list(value = list(list(list(
                      method = "equivValue", value = "366"))), score = 1L), 
                      list(value = list(list(list(method = "equivValue", 
                        value = "29.5", options = list(tolerance = "\\range", 
                          toleranceMinimum = 28L, toleranceMaximum = 31L)))), 
                        score = 1L), list(value = list(list(list(
                        method = "equivValue", value = "280", options = list(
                          tolerance = "\\range", toleranceMinimum = 266L, 
                          toleranceMaximum = 294L)))), score = 1L), 
                      list(value = list(list(list(method = "equivValue", 
                        value = "740000", options = list(decimalPlaces = "2", 
                          setDecimalSeparator = ".", setThousandsSeparator = list())))), 
                        score = 1L)))), widget_type = "response", 
            reference = "aaaaaaaa-aaaa-aaaa-aaaa-000000000001", type = "clozeformulaV2", 
            metadata = list(name = "Fill in maths"))), source = "", 
        workflow = list())), rendering_type = "assess"), description = "", 
        status = "published", title = "Test numerical_question", 
        settings = list(information_page = TRUE, page_content = ""))), 
        metadata = list(author_version = "4.2.0", date_created = "<date>", 
            created_by = "library(examiner)", content = "activity"))

# wiseflow_export_items handles text_only_question

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["data", "metadata"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["title", "status", "definition", "questions", "features"]
                }
              },
              "value": [
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["item"]
                },
                {
                  "type": "character",
                  "attributes": {},
                  "value": ["published"]
                },
                {
                  "type": "list",
                  "attributes": {
                    "names": {
                      "type": "character",
                      "attributes": {},
                      "value": ["template", "widgets"]
                    }
                  },
                  "value": [
                    {
                      "type": "character",
                      "attributes": {},
                      "value": ["dynamic"]
                    },
                    {
                      "type": "list",
                      "attributes": {},
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["feature"]
                            }
                          ]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["reference", "widget_type"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000002"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["response"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "widget_type", "reference", "type", "metadata"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["stimulus", "validation", "metadata", "shuffle_options", "options", "type", "ui_style"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Multiple Choice<\/strong> question (multiple_choice_question).<\/p>\n<p>What is the colour of blood?<\/p>"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["valid_response", "scoring_type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["value", "score"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "list",
                                      "attributes": {},
                                      "value": [
                                        {
                                          "type": "character",
                                          "attributes": {},
                                          "value": ["0"]
                                        }
                                      ]
                                    },
                                    {
                                      "type": "double",
                                      "attributes": {},
                                      "value": [1]
                                    }
                                  ]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["exactMatch"]
                                }
                              ]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["distractor_rationale_response_level"]
                                }
                              },
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {},
                                  "value": [
                                    {
                                      "type": "NULL"
                                    },
                                    {
                                      "type": "NULL"
                                    },
                                    {
                                      "type": "NULL"
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "logical",
                              "attributes": {},
                              "value": [true]
                            },
                            {
                              "type": "list",
                              "attributes": {},
                              "value": [
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Red<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["0"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>White<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["1"]
                                    }
                                  ]
                                },
                                {
                                  "type": "list",
                                  "attributes": {
                                    "names": {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["label", "value"]
                                    }
                                  },
                                  "value": [
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["<div>Blue<\/div>"]
                                    },
                                    {
                                      "type": "character",
                                      "attributes": {},
                                      "value": ["2"]
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["mcq"]
                            },
                            {
                              "type": "list",
                              "attributes": {
                                "names": {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["choice_label", "type"]
                                }
                              },
                              "value": [
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["upper-alpha"]
                                },
                                {
                                  "type": "character",
                                  "attributes": {},
                                  "value": ["horizontal"]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["response"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000002"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["mcq"]
                        },
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["name"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["Multiple responses"]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list",
                  "attributes": {},
                  "value": [
                    {
                      "type": "list",
                      "attributes": {
                        "names": {
                          "type": "character",
                          "attributes": {},
                          "value": ["data", "type", "widget_type", "reference"]
                        }
                      },
                      "value": [
                        {
                          "type": "list",
                          "attributes": {
                            "names": {
                              "type": "character",
                              "attributes": {},
                              "value": ["type", "content"]
                            }
                          },
                          "value": [
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["sharedpassage"]
                            },
                            {
                              "type": "character",
                              "attributes": {},
                              "value": ["<p>This is a <strong>Text Only Question<\/strong> (text_only_question) for testing.<\/p>\n<\/div>"]
                            }
                          ]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["sharedpassage"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["feature"]
                        },
                        {
                          "type": "character",
                          "attributes": {},
                          "value": ["aaaaaaaa-aaaa-aaaa-aaaa-000000000001"]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {
            "names": {
              "type": "character",
              "attributes": {},
              "value": ["author_version", "date_created", "created_by", "content"]
            }
          },
          "value": [
            {
              "type": "character",
              "attributes": {},
              "value": ["4.2.0"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["<date>"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["library(examiner)"]
            },
            {
              "type": "character",
              "attributes": {},
              "value": ["item"]
            }
          ]
        }
      ]
    }

# numerical_answer_canvas_to_wiseflow handles range_answer

    {
      "method": "equivValue",
      "value": "15",
      "options": {
        "tolerance": "\\range",
        "toleranceMinimum": 10,
        "toleranceMaximum": 20
      }
    }

# numerical_answer_canvas_to_wiseflow handles precision_answer

    {
      "method": "equivValue",
      "value": "3.14",
      "options": {
        "decimalPlaces": 2,
        "setDecimalSeparator": ".",
        "setThousandsSeparator": []
      }
    }

