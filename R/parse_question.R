#' Parse question fields from HTML generated from Rmd.
#'
#' See the example Rmd files for the various question types on how to specify
#' stimulus text and answer options, and to see where HTML is permitted.
#'
#' **If there are images in the question:**
#'
#' When targeting Canvas, images will be uploaded using the Canvas Files API.
#' You may optionally specify a `parent_folder_path`, default is the Files root.
#'
#' When targeting WISEflow, you have three options:
#'
#' * Specify `url_prefix` and `remote_dir` to have images uploaded to a web server.
#'   See `?wiseflow_images` for details.
#' * Specify `url_prefix` and manually move images to the web server.
#' * Set `ignore_images=TRUE`.
#'   Then images will be broken in WISEflow, but you can manually insert them later.
#'
#' @param rmd Name of R markdown file.
#' @param ignore_images Set this to TRUE to not copy generated or local images to a web server.
#'     Any such images will appear broken on import, and you must manually replace them.
#' @param url_prefix URL to root path on server, e.g. "https://example.org/~user/doc"
#'     Required to use images with WISEflow.
#' @param remote_dir Optional scp destination path, e.g. "user@server.example.org:~/public_html/doc"
#' @param self_contained Whether to render images inline (data URIs) or in external files (the default)
#' @param parent_folder_path Optional parent folder path for images in Canvas
#' @param abstain Text for an optional extra option (!) like "I wish to leave this question unanswered".
#'     Relevant where the exam system does not allow a test-taker to unselect all options.
#'
#' @return List representing a question of the given type according to
#'     \url{https://canvas.instructure.com/doc/api/quiz_questions.html}
#'
#' @examples
#' examiner:::parse_question(system.file("multiple_choice_question.Rmd",
#'                                       package = "examiner", mustWork = TRUE))
#' \dontrun{
#' examiner:::parse_question(
#'   rmd = system.file("multiple_penguin_question.Rmd", package = "examiner", mustWork = TRUE),
#'   url_prefix = "https://example.org/~user/test",
#'   remote_dir = "user@server.example.org:~/public_html/test")
#' }
parse_question <- function(rmd,
                           ignore_images = FALSE,
                           url_prefix = NULL,
                           remote_dir = NULL,
                           self_contained = FALSE,
                           parent_folder_path = NULL,
                           abstain = NULL) {
  # Avoid codetools complaints about "no visible binding for global variable".
  # The problem is that codetools doesn't understand the non-standard evaluation in dplyr verbs.
  . <- qt_exams <- NULL

  # Render with lightweight theme and put resulting images in a separate directory
  # render() returns output filename, which may differ from `rmd`,
  # e.g. substituting hyphens for spaces.
  html_name <- rmarkdown::render(rmd,
                    output_format = "html_fragment",
                    output_options = list(
                      self_contained = self_contained,
                      md_extensions = list("-smart"),
                      pandoc_args = list("--metadata=title=examiner_question",
                                         "--wrap=preserve",
                                         "--eol=lf")),
                    envir = new.env(),
                    quiet = TRUE)

  if (!ignore_images && is.null(url_prefix)) {
    canvas_move_images(html_name, parent_folder_path)
  } else {
    if (!ignore_images & !is.null(url_prefix))
      move_images(html_name, url_prefix, remote_dir)
  }
  readr::read_file(html_name) |>
    stringr::str_replace_all("\r\n", "\n") |>  # Normalize to LF line endings
    xml2::read_html() -> doc
  doc |>
    rvest::html_nodes("div#meta-information p") -> nodes_meta
  if (length(nodes_meta) == 0)
    stop(glue::glue("Meta-information not found in {rmd}! Did you forget the 'Meta-information' header? (Must be level 1 heading.)"))
  nodes_meta |>
    rvest::html_text() |>
    stringr::str_replace_all("\\b([a-z_]+):", "\\1") |>
    stringr::str_trim() |>
    stringr::str_split("\n") |>
    _[[1]] |>
    stringr::str_split("\\s", n = 2) |>
    purrr::map(stringr::str_trim) |>
    do.call(rbind, args = _) -> meta_matrix
  meta <- as.list(meta_matrix[,2])
  names(meta) <- meta_matrix[,1]

  q <- list()

  # Question name (used by Canvas, but not WISEflow) ----
  q$question_name <- rmarkdown::yaml_front_matter(rmd)$title

  # Question text as HTML ----

  # Question text is from div#question to immediately before div#solution or div#meta-information,
  # whichever comes first.
  # html_children() returns only _contained_ nodes, not following.

  # Note difference between HTML and XHTML: https://www.w3schools.com/tags/tag_br.asp
  # Wiseflow requires XHTML-style `<br />` as opposed to HTML-style `<br>`.
  # Canvas will autoconvert <br> to <br /> etc.

  re <- stringr::regex('<div id="question" .*?>\\s*<h1>Question</h1>(.*?)(<div id="answerlist"|<div id="meta-information"|<div id="solution")', dotall = TRUE)
  q$question_text <- doc |>
    as.character(options = "as_xml") |>  # Close empty tags (options= is an argument to write_xml())
    stringr::str_remove_all("&#13;") |>  # nuisance encoding for carriage return on Windows
    stringr::str_match(re) |>
    _[1,2] |>
    stringr::str_trim() |>
    stringr::str_replace("\r\n", "\n") |>
    # Recent knitr injects empty anchors to each code block line,
    # e.g. #cb1-2 for code block 1, line 2.
    # These get rendered as <a .../> by xml2::write_xml() via as.character above
    # but Canvas turns them into <a ...> although this is buggy HTML5.
    # However, we can cleanly prevent the problem:
    # https://regex101.com/r/nUrwYg/1/
    stringr::str_replace_all(r"((<a href="#cb[0-9]+-[0-9]+".*\/>))", r"(\1</a>)")
  if (is.na(q$question_text))
    stop(glue::glue("Question text not found in {rmd}! Did you forget the Question header? (Must be level 1 heading.)"))

  # Canvas specifics:
  # MATH: Either be Canvas admin and have a custom javascript load mathjax,
  # or use upmath.me as a workaround to get external img tags.
  # TODO: Check if browsers in 2024 support math automatically.

  # IMAGES: avoid data urls, see self_contained = FALSE above.

  # CONTENT SIZE LIMIT: 16 kB, 16384 bytes
  # Testing:
  if (FALSE) {
    # HTTP 200 OK
    question$question_text <- paste0(rep("a", times = 16384), collapse = "")
    canvas_api("quizzes", quiz$id, "questions") |>
      httr2::req_body_json(list(question = question)) |>
      httr2::req_perform() -> resp
    # HTTP 500 Internal Server Error
    question$question_text <- paste0(rep("a", times = 16385), collapse = "")
    canvas_api("quizzes", quiz$id, "questions") |>
      httr2::req_body_json(list(question = question)) |>
      httr2::req_perform() -> resp
  }
  # Question type as specified in the Meta-information section of the Rmd ----

  # Question types (scraped from the Edit Question dropdown list).
  #
  # How do I create a quiz with individual questions?
  # https://community.canvaslms.com/docs/DOC-26504-how-do-i-create-a-quiz-with-individual-questions#select_question_type
  #
  # How do I answer each type of question in a quiz?
  # https://community.canvaslms.com/docs/DOC-10582-421250757
  qt_string <- '
    <option value="multiple_choice_question">Multiple Choice</option>
    <option value="true_false_question">True/False</option>
    <option value="short_answer_question">Fill In the Blank</option>
    <option value="fill_in_multiple_blanks_question">Fill In Multiple Blanks</option>
    <option value="multiple_answers_question">Multiple Answers</option>
    <option value="multiple_dropdowns_question">Multiple Dropdowns</option>
    <option value="matching_question">Matching</option>
    <option value="numerical_question">Numerical Answer</option>
    <option value="calculated_question">Formula Question</option>
    <option value="essay_question">Essay Question</option>
    <option value="file_upload_question">File Upload Question</option>
    <option value="text_only_question">Text (no question)</option>'
  qt_html <- qt_string |> xml2::read_html() |> rvest::html_nodes("option")
  qt <- tibble::tibble(exams = NA_character_,
                       question_type = qt_html |> rvest::html_attr("value"),
                       description = qt_html |> rvest::html_text())
  # Comments explain how library(exams) uses these types:
  qt_exams <- list(
    schoice = "multiple_choice_question",  # Pick the one correct alternative (radio buttons)
    mchoice = "multiple_choice_question",  # Pick all correct alternatives (checkboxes)
    string = "short_answer_question",
    num = "numerical_question")
  # text_only_question displays purely informational text, not asking an actual question.
  if (rlang::has_name(qt_exams, meta$extype)) {
    q$question_type <- qt_exams[[meta$extype]]
  } else {
    q$question_type <- meta$extype
  }

  # Points awarded for correct answer. Required for people to score points.
  q$points_possible <- if (is.null(meta$expoints)) 1 else as.numeric(meta$expoints)

  # Shuffling defaults to FALSE in both Wiseflow and Canvas.
  q$shuffle <- purrr::pluck(meta, "exshuffle", .default = FALSE)

  # This if-monster maps out the question types I have implemented in Canvas.
  # (wiseflow_export_items() maps question types between Wiseflow and Canvas.)
  if (q$question_type != "text_only_question") {
    if (!(q$question_type %in% c("essay_question", "file_upload_question"))) {
      q$answers <- parse_answers(doc, meta, abstain)
      if (q$question_type == "matching_question")
        q$matching_answer_incorrect_matches <- attr(q$answers, "matching_answer_incorrect_matches")
    }
    for (i in c("correct", "incorrect", "neutral")) {
      comment <- doc |>
        rvest::html_nodes(glue::glue("div#{i}_comments > :not(:first-child)"))
      if (!all(rlang::is_empty(comment)))
        # Documentation bug: must use correct_comments_html instead of correct_comments.
        q[[glue::glue("{i}_comments_html")]] <- comment |>
          as.character() |>
          stringr::str_c(collapse = "\n")
    }
  }

  q$meta <- meta

  return(q)
}

#' Helper function to locate a question template file
#'
#' Question type names are taken from the Canvas LMS API at
#' `https://canvas.instructure.com/doc/api/quiz_questions.html#method.quizzes/quiz_questions.create`
#'
#' @param question_type Canvas API question type, e.g. "multiple_choice_question"
#' @return Path to template file
#' @keywords internal
#' @examples
#' examiner:::question_file("multiple_choice_question")
question_file <- function(question_type) {
  # First, try to find the file within the installed package
  result <- system.file("rmarkdown/templates", question_type, "skeleton/skeleton.Rmd", package = "examiner")

  # If the file does not exist in the system.file result (i.e., package likely not installed)
  if (!file.exists(result)) {
    # Fall back to using here() in development environment
    result <- here::here("inst/rmarkdown/templates",
                         question_type,
                         "skeleton/skeleton.Rmd")
  }
  if (!file.exists(result)) {
    stop(question_type, " example file does not exist: ", result)
  }
  return(result)
}
