#' Create a learnr tutorial file from an examiner quiz file
#'
#' @param quiz_file Path to the examiner quiz file (Rmd).
#' @return A learnr quiz object for inclusion in a learnr tutorial.
#' @export
learnr_quiz <- function(quiz_file) {
  # Get all the data
  md <- get_metadata(quiz_file)
  q <- md$metadata$questions |>
    purrr::map(\(section) section |>
          purrr::map(parse_question, self_contained = TRUE, .progress = TRUE) |>
          purrr::keep(\(x) x$question_type %in% c("schoice",
                                                  "multiple_choice_question",
                                                  "string",
                                                  "short_answer_question")) |>
          purrr::compact())
  # Data file to be included in the learnr file
  # and processed with examiner_to_learnr()
  ..data.. <- list(md = md, q = q)

  yaml_header <- yaml::as.yaml(list(title = md$metadata$title,
                                    output = "learnr::tutorial",
                                    runtime = "shiny_prerendered"))

  # An R code chunk for each section of the quiz
  sections <- r"---(

  ## {names(q)}

  ```{{r section_{seq_along(q)}, echo=FALSE}}
  rlang::inject(
    learnr::quiz(
      caption = r"({names(q)})",
      !!!(..data..$q[[{seq_along(q)}]] |>  purrr::map(examiner_to_learnr))))
  ```
  )---" |> glue::glue() |> stringr::str_c(collapse = "\n\n")

  template <- r"(
---
{yaml_header}
---

## Front page

{get_description(quiz_file) |>
  stringr::str_remove_all('class="section level2"') |>
  stringr::str_replace_all("<h[1-9]>", "<strong>") |>
  stringr::str_replace_all("</h[1-9]>", "</strong>") |>
  htmltools::HTML()}

```{{r setup, include = FALSE}}
examiner_to_learnr <- function(q) {{
  if (q$question_type %in% c("schoice", "multiple_choice_question")) {{
    return(rlang::inject(
             learnr::question(
               allow_retry = TRUE,
               text = q$question_text |>
                 stringr::str_remove('class="section level2"') |>
                 htmltools::HTML(),
               correct = q$correct_comments %||% "Correct!",
               incorrect = paste(q$incorrect_comments %||% "Incorrect",
                                 "\n\n",
                                 q$neutral_comments),
               random_answer_order = TRUE,
               !!!(q$answers |>
                     purrr::map(\(a)
                                learnr::answer(a$answer_html,
                                               correct = a$answer_weight > 0,
                                               message = a$comments_html))))))
  }} else {{
    # "string" or "short_answer_question"
    return(rlang::inject(
             learnr::question_text(
               allow_retry = FALSE,
               text = q$question_text |>
                 stringr::str_remove('class="section level2"') |>
                 htmltools::HTML(),
               correct = q$correct_comments %||% "Correct!",
               incorrect = paste(q$incorrect_comments %||% "Incorrect",
                                 "\n\n",
                                 q$neutral_comments,
                                 "\n\nThe correct answer is:\n\n",
                                 q$answers |>
                                   purrr::keep(\(a) a$answer_weight > 0) |>
                                   purrr::map_chr("answer_text") |>
                                   paste(collapse = "\n\n")),
               !!!(q$answers |>
                     purrr::map(\(a)
                                learnr::answer(a$answer_text,
                                               correct = a$answer_weight > 0,
                                               message = a$comments_html))))))
  }}
}}

..data.. <- {paste0(capture.output(dput(..data..)), collapse = "\n")}
```

{sections}
)"
  glue::glue(template) |>
    readr::write_file(quiz_file |> stringr::str_replace("\\.Rmd$", "_learnr.Rmd"))
}
