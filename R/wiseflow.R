# R template for WISEflow JSON export/import format.
# I exported a quiz from WISEflow to JSON, inspected the JSON,
# and experimented to figure out some pieces that could be omitted,
# such as `reference` uuid's.
#
# The template is an R list, which can be populated with sections of questions,
# and exported with jsonlite::write_json.
#
# Terminology:
#
# Item: A page with one or more questions. Also called a Section.
#   Question: An introductory text (`stimulus`)
#     Option: A possible answer (`label`) identified by a `value`
#   Validation: Which `value` is correct, and corresponding `score`.


#' Generate universally unique identifiers for widgets
#'
#' `uuid::UUIDgenerate()` gives nondeterministic output,
#' which poses a challenge to snapshot testing
#' e.g. with testthat::expect_snapshot().
#'
#' This wrapper function, make_uuid(), exists only to let us "mock" uuid generation
#' with a deterministic function in test-wiseflow.R.
#'
#' @return universally unique identifier
#' @keywords internal
make_uuid <- function() {
  uuid::UUIDgenerate()
}

#' Dealing with local or generated images in WISEflow
#'
#' @description
#' Note that WISEflow since spring 2024 does not accept inline images in import files.
#' Instead, you have three options, ranked in order of increasing convenience:
#'
#' * **Use only external images** residing on someone else's web server, or no images at all.
#' * **Edit questions after import to WISEflow,** copying images from your computer into the question editor.
#'   For R graphics output, this would involve manual knitting and then copying images.
#'   Make sure the output is deterministic, i.e. does not involve pseudorandom numbers.
#' * **Have library(examiner) copy images to a web server.**
#'   * Make sure you can access the web server e.g. using [WinSCP](https://winscp.net) or [Filezilla Client](https://filezilla-project.org/).
#'     You might need to use VPN.
#'   * Set up [passwordless ssh](https://www.strongdm.com/blog/ssh-passwordless-login).
#'     * Generate a private-public key pair on your local computer.
#'       I suggest using the default filename (`id_rsa` and `id_rsa.pub`, or `id_ed25519` and `id_ed25519.pub`) and no passphrase.
#'     * Copy the public key (the `.pub` file) to `~/.ssh/` on the web server.
#'       Set appropriate permissions on the .ssh folder and key file, see the `chmod` part of the guide linked above.
#'     * Verify that passwordless ssh works by running the following in a terminal:
#'
#'           ssh username@server.example.org pwd
#'
#'       replacing *server.example.org* with the web server name and *username* with your username on that server.
#'       The `pwd` command should print your working directory on the server, typically something like `/home/username`.
#'     * Verify that secure copy works by running the following in a terminal:
#'
#'           echo Test > test1234.txt
#'           scp test1234.txt username@server.example.org:~
#'           ssh username@server.example.org cat test1234.txt
#'
#'       adapting the username and server name as above.
#'
#'       The `echo ... > ...` command creates a small text file.
#'       The `scp` command copies the file to your home directory (`~`) on the server.
#'       The `ssh` command connects to the server and prints the contents of the copied file.
#'   * Figure out which folder on the server is served to the web.
#'     It might be `~/public_html`.
#'   * Figure out what URL the folder is served as.
#'     It might be something like `https://server.example.org/~username/`.
#'   * Use this information to specify `url_prefix` and `remote_dir` to `examiner::wiseflow_export_assignment()`.
#'
#' With passwordless ssh and scp in place, the following should run successfully if you provide your own values for `url_prefix` and `remote_dir`.
#'
#' ```{r remote, eval = FALSE}
#' rmds <- system.file(c("multiple_choice_question.Rmd", "multiple_penguin_question.Rmd"),
#'                     package = "examiner", mustWork = TRUE)
#' exam <- list("Test with images" = list("A section" = as.list(rmds)))
#' wiseflow_export_assignment(
#'   exam,
#'   filename = "From R markdown to WISEflow.json",
#'   url_prefix = "https://server.example.org/~username/dirname",
#'   remote_dir = "username@server.example.org:~/public_html/dirname",
#'   add_question_number = TRUE)
#' ```
#' @name wiseflow_images
NULL


# Insert elements which are always the same or can be inferred.
autocomplete_json <- function(x) {
  x$metadata <- rlang::list2(
    author_version = "4.2.0",  # As per WISEflow export on 2023-05-09
    date_created = as.integer(Sys.time()),  # Unix timestamp
    created_by = "Jon Olav Vik",
    content = "item",
  )
  for (i in seq_along(x$data)) {
    x$data[[i]]$definition = rlang::list2(
      template = "dynamic",
      widgets = rep(list(list(widget_type = "response")), length(x$data[[i]]$questions)),
    )
    x$data[[i]]$status = "published"
    for (j in seq_along(x$data[[i]]$questions)) {
      x$data[[i]]$questions[[j]]$type = "mcq"
      x$data[[i]]$questions[[j]]$widget_type = "response"
      x$data[[i]]$questions[[j]]$metadata = list(name = "Standard")
      x$data[[i]]$questions[[j]]$data$type = "mcq"
      # Math generates either span class="math inline" or "math display"
      if (x$data[[i]]$questions[[j]]$data$stimulus |> stringr::str_detect('span class="math ')) {
        x$data[[i]]$questions[[j]]$data$is_math = TRUE
      }
      x$data[[i]]$questions[[j]]$data$ui_style = rlang::list2(
        choice_label = "upper-alpha",
        type = "horizontal",
      )
      for (k in seq_along(x$data[[i]]$questions[[j]]$data$options)) {
        if (json$data[[i]]$questions[[j]]$data$options[[k]]$label |> stringr::str_detect('span class="math ')) {
          json$data[[i]]$questions[[j]]$data$is_math = TRUE
        }
        x$data[[i]]$questions[[j]]$data$options[[k]]$value = as.character(k - 1)
      }
      x$data[[i]]$questions[[j]]$data$validation$scoring_type = "exactMatch"
    }
  }
  return(x)
}

#' Convert numerical answer data from Canvas to WISEflow format
#'
#' @param a List or data frame as returned by `parse_answers()`
#'
#' @returns List as used in the `valid_response` or `alt_responses` fields
#'    of the JSON format used for export/import by WISEflow.
#' @keywords internal
#' @examples
#' examiner:::numerical_answer_canvas_to_wiseflow(
#'   list(
#'     answer_approximate = 1,
#'     numerical_answer_type = "precision_answer",
#'     answer_precision = 3
#'   )
#' )
numerical_answer_canvas_to_wiseflow <- function(a) {
  stopifnot(a$numerical_answer_type %in% c("exact_answer", "range_answer", "precision_answer"))
  # If "exact_answer with margin", hack it as a range_answer
  if (rlang::has_name(a, "answer_error_margin") &&
      !is.na(a$answer_error_margin)) {
    x <- as.numeric(a$answer_exact)
    margin <- as.numeric(a$answer_error_margin)
    a <- rlang::list2(
      numerical_answer_type = "range_answer",
      answer_range_start = as.character(x - margin),
      answer_range_end = as.character(x + margin),
    )
  }

  # If "exact_answer with precision", hack it as a range_answer
  if (rlang::has_name(a, "answer_precision") &&
      !is.na(a$answer_precision)) {

    n <- as.numeric(a$answer_precision)
    x <- a$answer_approximate |> as.numeric() |> signif(n)
    # Compute the margin based on the rounded value:
    # margin = 0.5 * 10^(floor(log10(abs(x))) - n + 1)
    order <- floor(log10(abs(x)))
    margin <- 0.5 * 10^(order - n + 1)

    a <- rlang::list2(
      numerical_answer_type = "range_answer",
      answer_range_start = as.character(x - margin),
      answer_range_end = as.character(x + margin)
    )
    # answer_precision is meant to be number of significant digits
    # For example, 3.14 or 3.14159 to...
    # one significant digit means [2.5, 3.5)
    # two significant digits means [3.05, 3.15)
    # three significant digits means [3.135, 3.145)
    # four significant digits means [3.1415, 3.1425)
  }

  if (a$numerical_answer_type == "exact_answer") {
    result <- rlang::list2(
      method = "equivValue",
      value = a$answer_exact,
    )
  } else if (a$numerical_answer_type == "range_answer") {
    result <- rlang::list2(
      method = "equivValue",
      value = a[c("answer_range_start", "answer_range_end")] |>
        unlist() |>
        as.numeric() |>
        mean() |>
        as.character(),  # Must be character or WISEflow says "Something went wrong"
      options = rlang::list2(
        tolerance = "\\range",
        toleranceMinimum = as.numeric(a$answer_range_start),
        toleranceMaximum = as.numeric(a$answer_range_end),
      ),
    )
  }
  return(result)
}

#' Make R list for Items export to WISEflow, to be passed to write_json().
#'
#' The `url_prefix` and `remote_dir` arguments are only needed if your question includes images.
#' See \code{\link{move_images}} for details on how these parameters work.
#'
#' The JSON format had to be reverse engineered, constructing R lists corresponding to exports from WISEflow.
#' This is an error-prone task, but at least the
#' [Data API error codes](https://help.learnosity.com/hc/en-us/articles/16458089770653-troubleshooting-Data-API#dataAPIErrorCodes)
#' are documented by Learnosity.
#'
#' Maybe also the Data API's list of [Item Bank Endpoints](https://help.learnosity.com/hc/en-us/articles/18356478270109-itembank-endpoints-Data-API) could be useful.
#'
#' @param items Named list of items: : `items[["item title"]][[i]]` is the .Rmd file name for question `i` in a given item.
#' @param ignore_images Set this to TRUE to not copy generated or local images to a web server.
#'     Any such images will appear broken on import, and you must manually replace them.
#' @param default_shuffle Whether to set shuffle=TRUE for questions if the setting is not stated.
#'   Default TRUE. Note that WISEflow and Canvas both default to FALSE.
#' @param add_question_number Whether to prepend "Question 1" etc to each question (WISEflow does not offer a global numbering).
#' @param url_prefix URL to root path on server, e.g. `https://example.org/~user/doc`.
#' @param remote_dir Optional scp destination path, e.g. `user@server.example.org:~/public_html/doc`.
#' @param abstain Text for an optional extra option (!) like "I wish to leave this question unanswered" for multiple-choice questions.
#'     This is useful because WISEflow offers to way to bring a multiple-choice question back to an unanswered state.
#'     `abstain=TRUE` is a shortcut for `abstain="I wish to leave this question unanswered"`.
#' @param penalty penalty=TRUE is a synonym for abstain=TRUE.
#'     If both `abstain` and `penalty` are given, `abstain` takes precedence.
#'
#' @return Nested list which can be passed to jsonlite::write_json(..., auto_unbox = TRUE) to produce a valid WISEflow import file.
#' @export
#'
#' @examples
#' # EXPORTING ITEMS
#'
#' items <- rlang::list2(
#'   "A simple multiple choice question" = rlang::list2(
#'     system.file("multiple_choice_question.Rmd", package = "examiner", mustWork = TRUE),
#'   ),
#'   "A multiple choice question with all the bells and whistles" = rlang::list2(
#'     system.file("xhtml-features.Rmd", package = "examiner", mustWork = TRUE),
#'     system.file("multiple_choice_question_random.Rmd", package = "examiner", mustWork = TRUE),
#'   ),
#' )
#' items |>
#'   wiseflow_export_items()
wiseflow_export_items <- function(items,
                                  ignore_images = FALSE,
                                  default_shuffle = TRUE,
                                  add_question_number = FALSE,
                                  url_prefix = NULL,
                                  remote_dir = NULL,
                                  abstain = NULL,
                                  penalty = NULL) {
  implemented_question_types <- tibble::tribble(
    ~canvas, ~wiseflow,
    "multiple_choice_question", "mcq",
    "multiple_answers_question", "mcq",
    "short_answer_question", "shorttext",
    "multiple_dropdowns_question", "clozedropdown",
    "fill_in_multiple_blanks_question", "clozetext",
    "matching_question", "association",
    "essay_question", "longtextV2",
    "file_upload_question", "fileupload",
    "numerical_question", "clozeformulaV2",
    # Homebrew hack, does not exist in Canvas:
    "fill_in_multiple_numerics_question", "clozeformulaV2",
    # https://authorguide.learnosity.com/hc/en-us/articles/360000445597-Shared-Passages
    "text_only_question", "passage",  # "feature", not "response"
  )

  # Avoid codetools complaints about "no visible binding for global variable".
  # The problem is that codetools doesn't understand the non-standard evaluation in dplyr verbs.
  . <- answer_html <- answer_weight <- blank_id <- qt_exams <- NULL

  if (is.null(abstain) && isTRUE(penalty)) {
    abstain <- TRUE
  }

  export <- function(rmd) {
    message(rmd)
    result <- parse_question(rmd = rmd,
                             ignore_images = ignore_images,
                             url_prefix = url_prefix,
                             remote_dir = remote_dir,
                             abstain = abstain)
    result$rmd <- rmd
    return(result)
  }

  q = items |>
    purrr::map(\(lst) purrr::map(lst, export))

  unsupported <- q |>
    purrr::list_flatten() |>
    purrr::discard(\(x) x$question_type %in% implemented_question_types$canvas) |>
    purrr::map(\(x) with(x, tibble::tibble(rmd, question_type))) |>
    purrr::list_rbind()
  if (length(unsupported) > 0) {
    c("",
      "",
      "The following question files are of an unsupported type:",
      "",
      utils::capture.output(print(unsupported))[-c(1, 3)],
      "",
      "Supported question types:",
      "",
      utils::capture.output(print(implemented_question_types))[-c(1, 3)]) |>
      paste(collapse = "\n") |>
      stop()
  }

  # There must be at least one question in each item
  q |>
    purrr::imap(function(it, n) {
      qt <- it |> purrr::map_chr("question_type")
      if (!any(qt != "text_only_question")) {
        cli::cli_abort(c("A WISEflow item must have at least one question,",
                         "not just passages (text_only_question).",
                         "Item '{n}' has no questions."))
      }
    })

  json = list(data = list())
  counter = 0
  json$metadata <- rlang::list2(
    author_version = "4.2.0",  # As per WISEflow export on 2023-05-09
    date_created = as.integer(Sys.time()),  # Unix timestamp
    created_by = "library(examiner)",
    content = "item",
  )
  for (i in seq_along(q)) {
    # Generate universally unique identifiers (uuid) for questions and features. Used here:
    # json$data[[i]]$definition$widgets
    # json$data[[i]]$questions[[jq]]$reference
    # json$data[[i]]$features[[jf]]$reference
    widgets = q[[i]] |>
      purrr::map(\(x) list(
        reference = make_uuid(),
        widget_type = ifelse(x$question_type == "text_only_question",
                             "feature",
                             "response")))
    widgets_q_ref = widgets |>
      purrr::keep(\(w) w$widget_type == "response") |>
      purrr::map_chr("reference")
    widgets_f_ref = widgets |>
      purrr::keep(\(w) w$widget_type == "feature") |>
      purrr::map_chr("reference")

    json$data[[i]] <- list(title = names(q)[i])
    json$data[[i]]$status = "published"
    json$data[[i]]$definition = list(
      template = "dynamic",
      widgets = widgets
    )
    json$data[[i]]$questions = list()
    # The WISEflow equivalent to text_only_question is a "passage",
    # which is a kind of "feature" as opposed to "question".
    # These have widget_type "feature" and "response", respectively.
    # These are stored in separate lists,
    # so we need separate index counters for the two lists.
    jq <- jf <- 0
    for (j in seq_along(q[[i]])) {
      if (q[[i]][[j]]$question_type == "text_only_question") {
        jf <- jf + 1
        json$data[[i]]$features[[jf]] = list(data = list())
        json$data[[i]]$features[[jf]]$type = "sharedpassage"
        json$data[[i]]$features[[jf]]$data$type = "sharedpassage"
        json$data[[i]]$features[[jf]]$data$heading = q[[i]][[j]]$question_name
        json$data[[i]]$features[[jf]]$data$content = q[[i]][[j]]$question_text
        json$data[[i]]$features[[jf]]$widget_type = "feature"
        json$data[[i]]$features[[jf]]$reference = widgets_f_ref[jf]

        # Math generates either span class="math inline" or "math display"
        if (json$data[[i]]$features[[jf]]$data$content |> stringr::str_detect('span class="math ')) {
          json$data[[i]]$features[[jf]]$data$is_math = TRUE
        }
      } else {
        jq <- jq + 1
        counter = counter + 1
        if (add_question_number) {
          header <- glue::glue('<p><mark>Question {counter}</mark></p>\n')
        } else {
          header = ""
        }

        json$data[[i]]$questions[[jq]] = list(data = list())
        json$data[[i]]$questions[[jq]]$data$stimulus = stringr::str_c(header, q[[i]][[j]]$question_text)
        json$data[[i]]$questions[[jq]]$widget_type = "response"
        json$data[[i]]$questions[[jq]]$reference = widgets_q_ref[jq]

        # Math generates either span class="math inline" or "math display"
        if (json$data[[i]]$questions[[jq]]$data$stimulus |> stringr::str_detect('span class="math ')) {
          json$data[[i]]$questions[[jq]]$data$is_math = TRUE
        }

        if (q[[i]][[j]]$question_type == "file_upload_question") {
          json$data[[i]]$questions[[jq]]$type = "fileupload"
          json$data[[i]]$questions[[jq]]$metadata = list(name = "File upload")
          json$data[[i]]$questions[[jq]]$data$type = json$data[[i]]$questions[[jq]]$type

          json$data[[i]]$questions[[jq]]$data <- within(json$data[[i]]$questions[[jq]]$data, {
            # File formats can be allowed with "allow_xyz", where xyz is one of:
            # pdf jpg gif png csv rtf txt xps zip ms_word ms_excel ms_powerpoint ms_publisher
            # open_office video matlab altera_quartus verilog c h s v cpp assembly labview
            # There seems to be no way to allow arbitrary file types, and thus no way to allow .Rmd.
            # Workaround: zip.

            # These are the WISEflow defaults:
            allow_pdf <- allow_jpg <- allow_gif <- allow_png <- photo_capture <- TRUE
            validation <- list(max_score = q[[i]][[j]]$points_possible)
            max_files <- 1
          })
        } else if (q[[i]][[j]]$question_type == "matching_question") {
          json$data[[i]]$questions[[jq]]$type = "association"
          json$data[[i]]$questions[[jq]]$metadata = list(name = "Pair elements")
          json$data[[i]]$questions[[jq]]$data$type = json$data[[i]]$questions[[jq]]$type

          # answer_match_left answer_match_right comments_html
          ans <- q[[i]][[j]]$answers |> purrr::map(tibble::as_tibble) |> purrr::list_rbind()
          distractors <- q[[i]][[j]]$answers |> attr("matching_answer_incorrect_matches") |> stringr::str_split_1("\n")

          json$data[[i]]$questions[[jq]]$data$stimulus_list <- ans$answer_match_left
          json$data[[i]]$questions[[jq]]$data$possible_responses <- c(ans$answer_match_right, distractors)
          json$data[[i]]$questions[[jq]]$data$validation = rlang::list2(
            allow_negative_scores = TRUE,
            scoring_type = "partialMatchV2",
            valid_response = rlang::list2(
              score = q[[i]][[j]]$points_possible,
              value = ans$answer_match_right,
            ),
            rounding = "none",
          )
          json$data[[i]]$questions[[jq]]$data$shuffle_options = rlang::`%||%`(default_shuffle, q[[i]][[j]]$shuffle)
        } else if (q[[i]][[j]]$question_type == "essay_question") {
          json$data[[i]]$questions[[jq]]$type = "longtextV2"
          json$data[[i]]$questions[[jq]]$metadata = list(name = "Essay")
          json$data[[i]]$questions[[jq]]$data$type = json$data[[i]]$questions[[jq]]$type

          json$data[[i]]$questions[[jq]]$data$submit_over_limit = TRUE
          # On by default in WISEflow, but not in Canvas:
          # json$data[[i]]$questions[[j]]$data$character_map = TRUE
          fmt <- list(
            default = c("bold", "italic", "underline",
                        "|", # Visual divider in toolbar
                        "unorderedList", "orderedList", "charactermap"),
            canvas = c(
              "bold", "italic", "underline", "unorderedList", "orderedList",
              "removeFormat", "superscript", "subscript", "indentIncrease",
              "indentDecrease", "table", "image", "characterMathMap", "undo",
              "redo")
          )
          json$data[[i]]$questions[[jq]]$data$formatting_options = fmt$canvas
        } else if (q[[i]][[j]]$question_type %in% c("numerical_question", "fill_in_multiple_numerics_question")) {
          json$data[[i]]$questions[[jq]]$type = "clozeformulaV2"
          json$data[[i]]$questions[[jq]]$metadata = list(name = "Fill in maths")
          json$data[[i]]$questions[[jq]]$data$type = json$data[[i]]$questions[[jq]]$type

          json$data[[i]]$questions[[jq]]$data$ui_style = list(type = "floating-keyboard")
          json$data[[i]]$questions[[jq]]$data$response_container = list(template = "")
          json$data[[i]]$questions[[jq]]$data$response_containers = list()
          json$data[[i]]$questions[[jq]]$data$is_math = TRUE

          if (q[[i]][[j]]$question_type == "numerical_question") {
            json$data[[i]]$questions[[jq]]$data$stimulus = q[[i]][[j]]$question_text
            json$data[[i]]$questions[[jq]]$data$template = "{{response}}"
            # q[[i]][[j]]$answers |> map(as_tibble) |> bind_rows()
            responses <- purrr::map(q[[i]][[j]]$answers, numerical_answer_canvas_to_wiseflow)
            json$data[[i]]$questions[[jq]]$data$validation = rlang::list2(
              scoring_type = "exactMatch",
              valid_response = rlang::list2(
                value = list(responses[1]),
                score = q[[i]][[j]]$points_possible,
              ),
              alt_responses = responses[-1] |>
                purrr::map(\(v) list(value = list(list(v)), score = q[[i]][[j]]$points_possible)),
            )
          } else {  # fill_in_multiple_numerics_question
            q[[i]][[j]]$question_text |>
              xml2::read_html() |>
              rvest::html_element("body") -> question_nodes
            stimulus_nodes <- question_nodes |>
              rvest::html_children() |>
              purrr::head_while(\(node) node |>
                                  as.character() |>
                                  stringr::str_detect(stringr::fixed("{{"), negate = TRUE))
            template_nodes <- question_nodes |>
              rvest::html_children() |>
              utils::tail(-length(stimulus_nodes))
            json$data[[i]]$questions[[jq]]$data$stimulus = stimulus_nodes |>
              as.character() |>
              paste0(collapse = "\n")
            re <- stringr::regex(r"(\{\{(.*?)\}\})")
            template_nodes |>
              as.character() |>
              paste0(collapse = "\n") |>
              stringr::str_match_all(re) |>
              _[[1]][,2,drop = TRUE] -> placeholders
            template_nodes |>
              as.character() |>
              paste0(collapse = "\n") |>
              stringr::str_replace_all(re, "{{response}}") -> template
            json$data[[i]]$questions[[jq]]$data$template = template
            responses <- purrr::map(q[[i]][[j]]$answers, numerical_answer_canvas_to_wiseflow)
            json$data[[i]]$questions[[jq]]$data$validation = rlang::list2(
              scoring_type = "partialMatchV2",
              rounding = "none",
              valid_response = rlang::list2(
                value = responses |> purrr::map(list),
                score = q[[i]][[j]]$points_possible,
              ),
            )
          }
        } else {
          json$data[[i]]$questions[[jq]]$data$validation = rlang::list2(
            valid_response = rlang::list2(
              value = rlang::list2(
                q[[i]][[j]]$answers |>
                  purrr::map_lgl(\(a) a$answer_weight != 0) |>
                  which() |>
                  (\(x) x - 1)() |>
                  as.character(),
              ),
              score = q[[i]][[j]]$points_possible,
            ),
          )
        }
        if (q[[i]][[j]]$question_type %in% c("fill_in_multiple_blanks_question", "multiple_dropdowns_question")) {
          q[[i]][[j]]$answers |>
            purrr::map(tibble::as_tibble) |>
            purrr::list_rbind() |>
            dplyr::mutate(blank_id = blank_id |> forcats::fct_inorder()) -> ans

          dd <- q[[i]][[j]]$question_type == "multiple_dropdowns_question"
          if (dd) {
            ans$answer_text |>
              split(ans$blank_id) |>
              unname() -> json$data[[i]]$questions[[jq]]$data$possible_responses
          }
          json$data[[i]]$questions[[jq]]$type = ifelse(dd, "clozedropdown", "clozetext")
          json$data[[i]]$questions[[jq]]$metadata = list(name = ifelse(dd, "Fill in text with drop-down", "Fill in text"))
          json$data[[i]]$questions[[jq]]$data$type = json$data[[i]]$questions[[jq]]$type

          # The rest is identical for clozedropdown and clozetext

          json$data[[i]]$questions[[jq]]$data$template <- json$data[[i]]$questions[[jq]]$data$stimulus |>
            stringr::str_replace_all("\\[.*?\\]", "{{response}}")
          json$data[[i]]$questions[[jq]]$data$stimulus <- ""
          # In this case, recycling elements from the shorter vector is okay
          # because of how "Match all possible responses" works.
          correct <- dplyr::filter(ans, answer_weight != 0)
          responses <- do.call(cbind, split(correct$answer_text, correct$blank_id)) |>
            tibble::as_tibble() |>
            dplyr::rowwise() |>
            dplyr::group_map(\(.x, .y) list(score = q[[i]][[j]]$points_possible,
                                            value = unname(as.vector(.x))))
          json$data[[i]]$questions[[jq]]$data$validation$valid_response <- responses[[1]]
          json$data[[i]]$questions[[jq]]$data$validation$alt_responses <- responses[-1]
          json$data[[i]]$questions[[jq]]$data$validation$allow_negative_scores = TRUE
          # https://authorguide.learnosity.com/hc/en-us/articles/360000440938-Cloze-Drop-Down#scoring
          # Partial Match - Each correct response element will be scored individually and the overall question score will be divided between responses.
          json$data[[i]]$questions[[jq]]$data$validation$scoring_type = "partialMatchV2"
          json$data[[i]]$questions[[jq]]$data$validation$match_all_possible_responses = TRUE
          json$data[[i]]$questions[[jq]]$data$validation$rounding = "none"
        }
        if (q[[i]][[j]]$question_type == "short_answer_question") {
          json$data[[i]]$questions[[jq]]$type = "shorttext"
          json$data[[i]]$questions[[jq]]$metadata = list(name = "Short text")
          json$data[[i]]$questions[[jq]]$data$type = "shorttext"
          ans <- q[[i]][[j]]$answers |> purrr::keep(\(a) a$answer_weight != 0) |> purrr::map_chr("answer_text")
          json$data[[i]]$questions[[jq]]$data$validation$valid_response$value = ans[1]
          json$data[[i]]$questions[[jq]]$data$validation$alt_responses = ans[-1] |>
            purrr::map(\(a) list(score = json$data[[i]]$questions[[jq]]$data$validation$valid_response$score,
                                 value = a))
          json$data[[i]]$questions[[jq]]$data$validation$allow_negative_scores = TRUE
          json$data[[i]]$questions[[jq]]$data$validation$scoring_type = "exactMatch"
        }
        if (q[[i]][[j]]$question_type %in% c("multiple_choice_question", "multiple_answers_question")) {

          if (isTRUE(abstain)) {
            json$data[[i]]$questions[[jq]]$data$validation$allow_negative_scores = TRUE
            n = length(q[[i]][[j]]$answers) - 1   # Number of non-abstain options
            json$data[[i]]$questions[[jq]]$data$validation$penalty = 1 / (n - 1) * q[[i]][[j]]$points_possible
            # The abstain option is placed last.
            # So we want one-based index n+1, but WISEflow uses zero-based indexing, so we use n+1-1
            json$data[[i]]$questions[[jq]]$data$validation$alt_responses = list(
              list(score = 0, value = list(as.character(as.integer(n))))
            )
          }

          json$data[[i]]$questions[[jq]]$data$metadata$distractor_rationale_response_level = q[[i]][[j]]$answers |>
            purrr::map("comments_html")
          json$data[[i]]$questions[[jq]]$data$shuffle_options = rlang::`%||%`(default_shuffle, q[[i]][[j]]$shuffle)
          json$data[[i]]$questions[[jq]]$data$options = q[[i]][[j]]$answers |>
            dplyr::bind_rows() |>
            dplyr::transmute(label = answer_html) |>
            dplyr::mutate(value = (1:dplyr::n()) - 1) |>
            purrr::transpose()
          json$data[[i]]$questions[[jq]]$type = "mcq"
          json$data[[i]]$questions[[jq]]$metadata = list(name = "Multiple responses")
          json$data[[i]]$questions[[jq]]$data$type = "mcq"
          json$data[[i]]$questions[[jq]]$data$ui_style = rlang::list2(
            choice_label = "upper-alpha",
            type = "horizontal",
          )
          for (k in seq_along(json$data[[i]]$questions[[jq]]$data$options)) {
            json$data[[i]]$questions[[jq]]$data$options[[k]]$value = as.character(k - 1)
            if (json$data[[i]]$questions[[jq]]$data$options[[k]]$label |> stringr::str_detect('span class="math ')) {
              json$data[[i]]$questions[[jq]]$data$is_math = TRUE
            }
          }
        }
        if (q[[i]][[j]]$question_type == "multiple_answers_question") {
          json$data[[i]]$questions[[jq]]$data$multiple_responses = TRUE
          # Without multiple responses, value is a one-element list of vector.
          # With multiple responses, value is just the vector. Curious.
          json$data[[i]]$questions[[jq]]$data$validation$valid_response$value = json$data[[i]]$questions[[jq]]$data$validation$valid_response$value[[1]]
          # WISEflow has exactMatch, partialMatch, partialMatchV2
          # The following mimicks Canvas settings
          # Note that partialMatch makes no sense without penalty, otherwise just pick all
          json$data[[i]]$questions[[jq]]$data$validation$allow_negative_scores = TRUE
          json$data[[i]]$questions[[jq]]$data$validation$scoring_type = "partialMatchV2"
          json$data[[i]]$questions[[jq]]$data$validation$penalty = json$data[[i]]$questions[[jq]]$data$validation$valid_response$score
          json$data[[i]]$questions[[jq]]$data$validation$min_score_if_attempted = 0.0
          json$data[[i]]$questions[[jq]]$data$validation$rounding = "none"
        }
        if (q[[i]][[j]]$question_type == "multiple_choice_question") {
          json$data[[i]]$questions[[jq]]$data$validation$scoring_type = "exactMatch"
        }
      }  # end if text_only_question
    }  # end for j
  }  # end for i
  return(json)
}

# Keys with empty values can be omitted: [], "", {} etc.
# Latest update sorting by "Latest update" stamps by UTC when I create questions inside WISEflow.

check_sections_structure <- function(sections) {
  tryCatch({
    stopifnot(is.list(sections),
              !is.null(names(sections)),
              sections |>
                purrr::map_lgl(\(x) is.character(x) && is.vector(x)) |>
                all(),
              all(unlist(sections) |> stringr::str_ends("\\.[Rr]md")))
  }, error = function(e) {
    msg <- paste(c("An assignment's sections must be specified as a named list of character vectors:",
                   'list(section_title_1 = c("question1.Rmd", "question2.Rmd", ...).',
                   "You had:",
                   yaml::as.yaml(sections)),
                 collapse = "\n")
    stop(msg, call. = FALSE)
  })
}

#' Export assignment for WISEflow in JSON format based on a nested list with question Rmd file names.
#'
#' The `url_prefix` and `remote_dir` arguments are only needed if your question includes images.
#' See \code{\link{move_images}} for details on how these parameters work.
#'
#' @param title Title of the assignment.
#' @param sections Named list of character vectors specifying the assignment's sections (called "items" in WISEflow): `sections$section_title_1[i]` is the .Rmd file name for question `i` in the item titled "section_title_1".
#'   For compatibility with Canvas, `sections` can also be just a character vector, in which case it's placed in a section called "Item 1".
#' @param filename Name of generated file, ending in .json.
#' @param info_page String of html for information page.
#' @param ignore_images Set this to TRUE to not copy generated or local images to a web server.
#'     Any such images will appear broken on import, and you must manually replace them.
#' @param default_shuffle Whether to set shuffle=TRUE for questions if the setting is not stated.
#'   Default TRUE. Note that WISEflow and Canvas both default to FALSE.
#' @param add_question_number Whether to prepend "Question 1" etc to each question (WISEflow does not offer a global numbering).
#' @param url_prefix URL to root path on server, e.g. `https://example.org/~user/doc`
#' @param remote_dir Optional scp destination path, e.g. `user@server.example.org:~/public_html/doc`
#' @param abstain Text for an optional extra option (!) like "I wish to leave this question unanswered".
#'     Relevant where the exam system does not allow a test-taker to unselect all options.
#' @param penalty penalty=TRUE is a synonym for abstain=TRUE.
#'     If both `abstain` and `penalty` are given, `abstain` takes precedence.
#'
#' @return Invisibly, the R list which was converted to JSON and saved to file.
#'
#' @export
#'
#' @examples
#' \dontrun{
#' sections <- list(
#'     "B is first" = c("question_type/multiple_choice_question.Rmd"),
#'     "A is second" = "ÆØÅ.Rmd"
#' )
#' wiseflow_export_assignment("Test", sections, "test_assignments.json", default_shuffle = TRUE)
#' }
wiseflow_export_assignment <- function(title,
                                       sections,
                                       filename,
                                       info_page = NULL,
                                       ignore_images = FALSE,
                                       default_shuffle = TRUE,
                                       add_question_number = FALSE,
                                       url_prefix = NULL,
                                       remote_dir = NULL,
                                       abstain = NULL,
                                       penalty = NULL) {

  if (is.character(sections))
    sections <- list("Item 1" = sections)

  check_sections_structure(sections)

  asg <- list(sections) |> rlang::set_names(title)

  it <- asg |> purrr::map(\(f) wiseflow_export_items(f,
                                                     ignore_images = ignore_images,
                                                     default_shuffle = default_shuffle,
                                                     add_question_number = add_question_number,
                                                     url_prefix = url_prefix,
                                                     remote_dir = remote_dir,
                                                     abstain = abstain,
                                                     penalty = penalty))

  json <- list(data = list(),
               metadata = it[[1]]$metadata |> utils::modifyList(list(content = "activity")))
  for (i in seq_along(it)) {
    json$data[[i]] <- rlang::list2(
      data = rlang::list2(
        items = it[[i]]$data,
        rendering_type = "assess",
      ),
      description = "",
      status = "published",
      title = names(it)[i],
      # Can perhaps be omitted to use defaults
      settings = rlang::list2(
        "go_to_first_question" = TRUE,
        "information_page" = FALSE,
        "negative_scores" = TRUE,
        "no_backward_navigation" = FALSE,
        # "page_content" = NULL,
        "reading_mode" = FALSE,
        "reading_time" = 10,
        "shuffle_sections" = FALSE,
        "warning_on_section_change" = TRUE,
        "warning_time" = 10,
      ),
    )
    for (j in seq_along(json$data[[i]]$data$items)) {
      # Elements required even if they are empty
      json$data[[i]]$data$items[[j]]$source = ""
      json$data[[i]]$data$items[[j]]$workflow = list()
    }  # end for j
  }  # end for i
  if (!is.null(info_page)) {
    json$data[[1]]$settings <- rlang::list2(
      "information_page" = TRUE,
      "page_content" = info_page,
    )
  }

  jsonlite::write_json(json, filename, auto_unbox = TRUE, digits = NA)

  import_url <- "https://europe.wiseflow.net/author/assignments/import-status"
  if (interactive()) {
    # absolute path
    abs_filename <- normalizePath(filename, mustWork = TRUE)
    tryCatch({
      clipr::write_clip(abs_filename)
      message("Full path to WISEflow import file copied to clipboard.")
    }, error = function(e) {
      r"(
      Unable to write path to clipboard:
      {e$message}

      WISEflow import file is here:
      {abs_filename}
      )" |>
        glue::glue() |>
        warning()
    })
    message("Import it here (you may have to log in first):")
    message(cli::style_hyperlink(import_url, import_url))
  }
  return(invisible(json))
}

