Type: Package
Package: examiner
Title: Generate Canvas or WISEflow quiz/exam questions from R markdown
Version: 0.1.0
Author: Jon Olav Vik
Maintainer: Jon Olav Vik <jon.vik@nmbu.no>
Description: Each question goes in a separate Rmd file.  A Canvas quiz is
    specified by a character vector of Rmd file names.  A WISEFLOW exam
    (assignment) is specified as a list of lists of lists: Level 1 name is
    for the assignment.  Level 2 names are for items (sections).  Level 3
    does not have names; the values specify the Rmd file for each
    question.
License: AGPL (>= 3)
Depends:
    R (>= 4.1.0)
Imports: 
    base64enc,
    cli,
    clipr,
    curl,
    digest,
    dplyr,
    forcats,
    glue,
    htmltools,
    httr2,
    lubridate,
    miniUI,
    purrr,
    readr,
    rlang,
    rmarkdown,
    rvest,
    shiny,
    stringi,
    stringr,
    tibble,
    tidyr,
    utils,
    uuid,
    xml2
Suggests: 
    chromote,
    fs,
    gganimate,
    ggplot2,
    gifski,
    here,
    highlighter,
    jsonlite,
    knitr,
    mockr,
    palmerpenguins,
    reticulate,
    rstudioapi,
    selenider,
    testthat (>= 3.0.0),
    vembedr,
    whisker,
    withr,
    yaml
VignetteBuilder: 
    knitr
Biarch: TRUE
Config/testthat/edition: 3
Encoding: UTF-8
LazyData: true
Roxygen: list(markdown = TRUE)
RoxygenNote: 7.3.2
URL: https://arken.nmbu.no/~jonvi/examiner, https://gitlab.com/jonovik/examiner
