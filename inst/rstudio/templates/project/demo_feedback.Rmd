# Question

:::{style="background-color: #ffdddd"}
This question demonstrates the **detailed feedback features of Canvas**.
You won't see a difference in WISEflow, but in Canvas the test-taker will get feedback tailored to the answer option they selected.
Study the Rmd source code to see how the feedback is specified.
:::

The following mathematical expression is known as *Stirling's formula*:

$$
\sqrt{2 \pi n} (\frac{n}{e})^n
$$

It is a very good approximation to one of the [special functions](https://en.wikipedia.org/wiki/Special_functions) in mathematics.
Which one?

## Answerlist

* The factorial, $n!$
* The natural logaritm, $\ln$
* The arctangent function, $\arctan$

# Solution

## Answerlist

* Yes, the factorial function is well approximated by Stirling's formula, from $n=1$ and upwards.
* No, the logaritm functions grow very slowly, with a constant amount per multiplication by a given number.
* The arctangent returns the angle $\theta$ which has a given value of $\frac{\sin(\theta)}{\cos(\theta)}$.
  For uniqueness, it is usually restricted to the interval $[-\pi, +\pi]$.

## correct_comments

Very good!
If you want to derive the formula yourself, you can approximate the sum 
$\ln(n!)=\sum _{j=1}^{n}\ln j$
with the integral
$\sum _{j=1}^{n}\ln j\approx \int _{1}^{n}\ln x\,{\rm {d}}x=n\ln n-n+1$
and take it from there.

## incorrect_comments

Don't despair!
Hint: Notice that *n* to the *n*th power means that this is a function that grows extremely fast.

## neutral_comments

The term "special functions" is defined by custom and common agreement, not by any clear mathematical property.

# Meta-information

extype: multiple_choice_question
exsolution: 100
