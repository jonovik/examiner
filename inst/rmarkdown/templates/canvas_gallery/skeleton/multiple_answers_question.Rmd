# Question

This is a **Multiple Answers** question (multiple_answers_question).

Which colours describe blood?
Check all that apply.

## Answerlist

* Red
* White
* Blue

# Solution

## Answerlist

*
*
* Royalty are sometimes said to have "blue blood", so we'll accept this one too.

# Meta-information

extype: multiple_answers_question
exsolution: 101