Left side (first level of list) and right side (second level) must be plain text.
Comments (optional third level; displayed in case of wrong answer) can be HTML.

# Question

This is a **Matching** question (matching_question).

Please pick the traditional colour for each flower:

## Answerlist

* roses
  * red
    * So, you couldn't pick the **correct** color for roses? You'd better read up on your poetry!  
      <img src="https://arken.nmbu.no/~jonvi/stin100/timeplan-mazemap.gif" width="40%">
* violets
  * blue
    * Yeah, that's a trick question. Check your poetry book and try again.
* DISTRACTORS
  * white
  * green
  * violet

# Meta-information

extype: matching_question
