# Question

This is a **Multiple Dropdowns** question (multiple_dropdowns_question).

Please complete this line from "Dear Prudence" by the Beatles:

The sun is [position], the sky is [color].

## Answerlist

* position
  * up
  * down
    * I'm afraid this is not a nighttime song.
  * out
* color
  * blue
  * green
    * While the Beatles did sometimes compose songs 
      under the influence of drugs, 
      this song does not claim the sky to be green.
  * the limit
    * We'll accept this answer too for testing purposes.

# Meta-information

extype: multiple_dropdowns_question
exsolution: 100 101