# Question

This is a **Fill In Multiple Blanks** question (fill_in_multiple_blanks_question).

Please complete this line from "Dear Prudence" by the Beatles:

The sun is [position], the sky is [color].

## Answerlist

* position
  * up
    * Comment to "up".
* color
  * blue

# Meta-information

extype: fill_in_multiple_blanks_question
