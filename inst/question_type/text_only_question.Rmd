# Question

This is a **Text Only Question** (text_only_question).

It is purely informational and not really a question.
You don't have to make any kind of response.

Here's a list of all the question types in Canvas.
(Note that this is "Old" Quizzes, not "New Quizzes" aka Quizzes.Next.
The latter has no API, results export or SpeedGrader yet.)

Label and `question_type` | Description
---|---
Multiple Choice <br> `multiple_choice_question` | Enter your question and multiple answers, then select the one correct answer.
Multiple Answers <br> `multiple_answers_question` | This question will show a checkbox next to each answer, and the student must select ALL the answers you mark as correct.
Fill In the Blank <br> `short_answer_question` | Enter your question text, then define all possible correct answers for the blank. Students will see the question followed by a small text box to type their answer.
Fill In Multiple Blanks <br> `fill_in_multiple_blanks_question` | Enter your question, specifying where each blank should go. Then define the possible correct answer for each blank. Students must type correct answers into text boxes at each blank. [...] In the box below, every place you want to show an answer box, type a reference word (no spaces) surrounded by brackets (i.e. "Roses are [color1], violets are [color2]")
Multiple Dropdowns <br> `multiple_dropdowns_question` | Enter your question, specifying where each dropdown should go. Then define possible answers for each dropdown, with one correct answer per dropdown. [...] In the box below, every place you want to show an answer box, type a reference word (no spaces) surrounded by brackets (i.e. "Roses are [color1], violets are [color2]")
Matching <br> `matching_question` | Build pairs of matching values. Students will see values on the left and have to select the matching value on the right from a dropdown. Multiple rows can have the same answer, and you can add additional distractors to the right side.
Numerical Answer <br> `numerical_question` | Define the correct answer as any number within a range, or a number plus or minus some error margin. Student will be given an empty text box to type their numerical answer.
Essay Question <br> `essay_question` | Students will be given a text field to compose their answer.
File Upload Question <br> `file_upload_question` | Students will be able to upload a file for their answer.
Text (no question) <br> `text_only_question` | This "question" will not be scored, but can be useful for introducing a set of related questions.
True/False <br> `true_false_question` | (Not included, easily replaced by multiple_choice_question.)
Formula Question <br> `calculated_question` | (Randomized numerical exercises. Not included yet, can be done with R.)

# Meta-information

extype: text_only_question
