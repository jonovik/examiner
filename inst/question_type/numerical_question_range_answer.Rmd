# Question

This is a **Numerical Answer** question (numerical_question).

How many days are there in a year?

## Answerlist

* 365 - 366

# Solution

## Answerlist

* Yes.
  Most years have 365 days, while leap years have 366 days.

# Meta-information

extype: numerical_question
