Note: Wiseflow allows only a limited set of file types, though zip can be used as a workaround.
Currently, library(examiner) does not support changing the defaults (no limitation on Canvas, pdf/jpg/gif/png/photo on WISEflow).

# Question

This is a **File Upload Question** (file_upload_question).

Please upload a file of your own choosing.

# Meta-information

extype: file_upload_question
