---
title: "Multiple choice question with R code and figure"
---

# Question

The penguin community at [Palmer Station](https://www.google.com/maps/place/Palmer+Station,+Antarctica/@-64.774125,-64.054375,15z/) in Antarctica includes three species of penguin.

```{r echo = FALSE, message = FALSE, fig.width = 7, fig.height = 4}
library(tidyverse)
library(palmerpenguins)
penguins |> 
  drop_na(body_mass_g, flipper_length_mm) |> 
  ggplot(aes(x = body_mass_g, 
             y = flipper_length_mm, 
             colour = species)) +
  geom_point() +
  expand_limits(x = 0, y = 0) +
  labs(x = "Body mass (g)", y = "Flipper length (mm)") +
  theme_classic(base_size = 20)
penguins |> 
  drop_na(flipper_length_mm, body_mass_g) |> 
  summarise(mean_flipper_per_mass = mean(flipper_length_mm / body_mass_g),
            .by = species) |> 
  arrange(mean_flipper_per_mass) |> 
  pull(species) -> answers
```

Which species of penguin has the lowest ratio of flipper length to body mass on average?

## Answerlist

```{r echo = FALSE, results = "asis"}
glue::glue("* {answers}")
```

# Meta-information

extype: multiple_choice_question
exsolution: 100
