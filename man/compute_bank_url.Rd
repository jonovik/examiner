% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/canvas.R
\name{compute_bank_url}
\alias{compute_bank_url}
\title{Compute question bank URL}
\usage{
compute_bank_url(x)
}
\arguments{
\item{x}{A character string representing either a bank URL, an ID, or the title of a question bank.}
}
\value{
A character string containing the full URL of the specified question bank.
}
\description{
If \code{x} is already a question bank URL, returns \code{x}.
If \code{x} is an integer, constructs a question bank URL by prefixing \if{html}{\out{<code>}}https://example.instructure.com/course/1234/question_banks/\if{html}{\out{</code>}} to \code{x}.
If \code{x} is a question bank title, read the list of question banks and return the URL of the matching question bank.
}
\details{
The title must match exactly one question bank, otherwise an error will list the duplicate matches.
}
\keyword{internal}
