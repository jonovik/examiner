0:00:00.160,0:00:05.920
Hei, og velkommen til denne veiledningen om hvordan du 
importerer filer fra R Markdown til WISEflow.

0:00:05.920,0:00:08.800
Jeg vil bla ned til installasjonsdelen, 
der det står at du må ha

0:00:08.800,0:00:14.840
R og RStudio installert på maskinen din. 
Du må også installere `remotes`-biblioteket

0:00:14.840,0:00:19.800
og `examiner`-biblioteket. Jeg har allerede 
alt dette installert, så jeg vil hoppe over denne delen. Men

0:00:19.800,0:00:26.160
du kan bare kopiere disse kodene og lime dem inn 
i konsollen for å installere dem. Jeg vil hoppe over til

0:00:26.160,0:00:31.400
"Kom i gang"-guiden og bla ned til "Lag 
et Quizzes using examiner-prosjekt," der det

0:00:31.400,0:00:38.800
forklares hvordan du kan opprette et nytt prosjekt. 
Jeg vil vise deg i R. Du kan gå til *File*,

0:00:38.800,0:00:46.880
klikke på *New Project*, deretter *New Directory*, 
og bla ned til *Quizzes using examiner*.

0:00:46.880,0:00:52.440
Her kan vi se den samme konfigurasjonsboksen 
som vi hadde i R. Vi må velge et katalognavn,

0:00:52.440,0:00:57.280
som bare er navnet du vil gi prosjektet, og 
du må velge hvor du vil lagre prosjektet.

0:00:57.280,0:01:04.040
Jeg vil bare kalle dette "test," og jeg vil 
gå gjennom hvordan du fyller ut de gjenværende dialogboksene

0:01:04.040,0:01:11.160
til høyre siden vi lager en WISEflow-oppsett. 
Så blar vi ned til denne konfigurasjonsveiledningen.

0:01:11.160,0:01:17.240
For å bruke WISEflow, må du ha en pålogging på 
WISEflow.net. Jeg har allerede logget inn på WISEflow

0:01:17.240,0:01:23.960
her, som du kan se, og du må gjøre 
det samme. Quizzes som bare bruker bilder som er

0:01:23.960,0:01:29.920
åpent tilgjengelige på nettet vil fungere med en gang, men 
for å inkludere grafikk fra R eller lokale bilder,

0:01:30.520,0:01:34.960
må du ha passordløs tilgang til en 
webserver der `examiner` kan laste opp

0:01:34.960,0:01:41.400
bildene dine. Det krever en engangsoppsett av SSH-nøkler. 
Videre må du for hvert prosjekt

0:01:41.400,0:01:47.600
fortelle `examiner` hvor bildene dine skal lagres. 
Se WISEflow-fanen for en detaljert gjennomgang.

0:01:48.360,0:01:53.160
Videre står det at hvis du bestemmer deg for å 
hoppe over dette og godta alle standardinnstillinger i New Project

0:01:53.160,0:02:00.160
veiviseren, vil du fortsatt kunne prøve ut WISEflow-filer 
uten R-genererte bilder og kan utforske

0:02:00.160,0:02:07.400
formatet på quiz- og spørsmålsfilene. 
De snakker om disse to konfigurasjonene

0:02:07.400,0:02:15.160
her. Hvis vi går tilbake til veiledningen, står det mer 
om hvordan du kan finne din webroot-katalog,

0:02:15.160,0:02:23.000
og den har veiledninger om hvordan du sjekker 
at du har SSH og SCP og hvordan du setter opp

0:02:23.000,0:02:29.360
SSH-nøkler. Det vil være en veiledning for dette i 
en egen video. Vi skal bare se på hvordan du

0:02:29.880,0:02:37.920
spesifiserer "Image URL root" og "SCP upload 
image root," som er de to feltene her. Her

0:02:37.920,0:02:44.200
kan du se en forklaring på hva dette er 
og eksempler på hvordan det kan se ut. Jeg vil

0:02:44.200,0:02:59.840
bruke nesten det samme, men med en testversjon 
i stedet for denne kurskoden.

0:03:14.880,0:03:23.160
Nå kan vi bare klikke på *Create Project*, 
og vi kan ta en titt på dette `.Renviron`-

0:03:23.160,0:03:28.720
vinduet. Vi kan se at dette er det samme 
som det jeg fylte ut i de to

0:03:28.720,0:03:35.320
dialogboksene til høyre. Vi kan gå 
tilbake til quizen og tilbake til veiledningen.

0:03:35.320,0:03:40.160
Klar. Ditt nye prosjekt åpnes med en 
eksempelfil for quiz i RStudio-editoren din.

0:03:40.160,0:03:44.280
Hovedinnholdet vil bli forsiden 
av quizen. `questions`-feltet

0:03:44.280,0:03:50.320
i YAML-headeren øverst spesifiserer spørsmålene: 
en `.Rmd`-fil per spørsmål. Vi

0:03:50.320,0:03:59.280
kan se her at all denne teksten vil bli 
forsiden av quizen, og øverst her,

0:03:59.280,0:04:06.480
hvor det står "questions," vil disse tre `.Rmd`-filene 
være spørsmålene i quizen.

0:04:06.480,0:04:11.000
La oss gå tilbake igjen. Først kan vi gjøre 
en testknit. Vi kan bare klikke på *Knit*-

0:04:11.000,0:04:16.960
knappen for å kompilere en enkelt HTML-fil 
fra quizfilen og alle spørsmålsfilene.

0:04:17.840,0:04:29.720
Nå kan vi bare klikke på *Knit*, og her har vi 
fått denne HTML-filen med quizen. Vi kan se at

0:04:30.400,0:04:46.040
denne teksten er forsiden, 
og vi har tre spørsmål.

0:04:46.040,0:04:52.240
Så kan vi bla ned til "Export to WISEflow," 
der det står at R-genererte bilder ikke vil

0:04:52.240,0:04:58.040
fungere før du konfigurerer `examiner`-prosjektet 
ditt for WISEflow som beskrevet ovenfor. Så du

0:04:58.040,0:05:05.240
må sette opp en SSH-nøkkel, som jeg nevnte, vil 
komme i en egen video. Nå må vi endre

0:05:05.240,0:05:10.320
utdataformatet for quizfilen til `output: 
examiner::wiseflow` ved å endre den linjen i

0:05:10.320,0:05:20.920
YAML-headeren øverst i quizfilen. Vi må 
kommentere ut denne og avkommentere denne.

0:05:20.920,0:05:33.760
Så kan vi knit igjen. Her kan vi se at vi har fått 
en beskrivelse av hva vi skal gjøre videre. Det står at

0:05:33.760,0:05:40.520
den fullstendige banen til WISEflow-importfilen 
har blitt kopiert til utklippstavlen. Den er på utklippstavlen,

0:05:40.520,0:05:49.320
men vi har den også her. Gå hit for å 
importere, så nå kan vi klikke på *Import*,

0:05:49.320,0:05:59.640
velge filfanen, velge WISEflow, 
velge filen, og deretter finne filen din,

0:06:03.840,0:06:07.920
som bør være `.json`-filen 
kalt "sample_quiz" med mindre du

0:06:07.920,0:06:15.120
har gitt den et annet navn. Så velger vi å laste opp,

0:06:15.120,0:06:26.400
og du må forhåndsvise filinnholdet 
og deretter importere igjen og bekrefte.

0:06:26.400,0:06:35.680
Nå vil det ta noen sekunder å fullføre. 
Du må kanskje laste inn siden din på nytt,

0:06:35.680,0:06:46.280
og det er fullført. Så vi kan klikke her 
og åpne elementet i *Assignments*,

0:06:46.280,0:06:53.680
og deretter kan vi forhåndsvise det hvis vi klikker 
på dette *Eye*-ikonet og forhåndsviser det som en oppgave,

0:06:53.680,0:07:05.640
for eksempel. Her kan vi se det første 
spørsmålet, det andre og det tredje. Her

0:07:05.640,0:07:13.360
har vi også denne animasjonen, 
som er mulig på grunn av SSH-oppsettet.

0:07:13.360,0:07:18.680
Så kan vi gå tilbake til veiledningen vår og gå til 
neste trinn, der det står at du kan

0:07:18.680,0:07:24.400
redigere quiz- og spørsmålsfilene for å lage din 
egen quiz, og det er en veiledning om hvordan du kan

0:07:24.400,0:07:32.960
opprette nye spørsmålsfiler. Jeg vil vise deg 
dette i R. Du kan gå til *File*, *New File*,

0:07:32.960,0:07:40.280
*R Markdown*, og deretter fra *Templates*, 
kan du se alle disse examiner-maler for

0:07:40.280,0:07:47.280
mulige spørsmåls typer du kan bruke. La oss prøve 
med et flervalgsspørsmål, for eksempel,

0:07:47.280,0:08:04.800
og så må vi lagre det. Vi kan kalle det 
"multiple_test," og så går vi tilbake til hoved

0:08:04.800,0:08:29.440
quizen og legger det til her som et spørsmål. Hvis vi så 
prøver å knit igjen, og deretter importere denne nye quizen,

0:08:51.720,0:09:00.720
og tar en titt på denne nye quizen, 
kan vi se at vi har dette nye flervalgsspørsmålet

0:09:00.720,0:09:12.200
her på samme side som det 
første siden det er under samme header.

0:09:12.200,0:09:18.480
Så kan vi gå tilbake til veiledningen vår igjen, 
der det står at du kan bruke R og mange Markdown-

0:09:18.480,0:09:25.440
og HTML-funksjoner. Se den tredje spørsmålsfilen 
i eksempelet quiz for detaljer. La oss ta en

0:09:25.440,0:09:31.480
titt på denne filen siden det er den tredje 
hvis vi ser bort fra den jeg la til, som vi

0:09:31.480,0:09:40.920
kan finne her. Det er mange eksempler på 
hvordan du kan formatere for video og animasjon,

0:09:40.920,0:09:49.320
som jeg viste deg i WISEflow-quizen. Så står det 
at du kan inkludere bilder fra filsystemet ditt

0:09:49.320,0:09:54.720
samt grafikk laget med R, 
som jeg nevnte i starten. Du trenger

0:09:54.720,0:10:02.120
passordløs SSH-tilgang til en webserver, 
som beskrevet i konfigurasjonen ovenfor.

0:10:03.600,0:10:08.800
Det er også andre spørsmåls typer 
enn flervalg, som jeg viste deg. Disse er

0:10:08.800,0:10:17.400
alle malene som er tilgjengelige for øyeblikket. 
Det var alt. Lykke til med å utforske `examiner`.
