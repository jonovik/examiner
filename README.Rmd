---
title: "Home - examiner"
output:
  html_document:
    keep_md: yes
    theme:
      version: 5
---

# examiner: Empowering educators [![examiner website](man/figures/logo.svg)](https://arken.nmbu.no/~jonvi/examiner){style="float:right;"}

<!-- badges: start -->
[![Lifecycle: stable](https://img.shields.io/badge/lifecycle-stable-brightgreen.svg)](https://lifecycle.r-lib.org/articles/stages.html#stable)
<!-- badges: end -->

**`library(examiner)` generates Canvas quizzes or WISEflow import files from R markdown.**

Use R markdown to infuse your questions with an engaging blend of storytelling, data analysis and visualization.
Upload your quizzes automatically to the Canvas learning management system, or import them to the WISEflow exam system.

No more tedious copying of figures and tables into a web interface where every click takes seconds.
Don't be locked in by proprietary systems without proven portability.

* Easily reuse previous exams as practice quizzes!
* Easily remix and revise your carefully designed exercises!
* Work with your favourite power tools on your own computer!
  * Quickly prototype and re-render exercises in RStudio.
  * Need to change a word in many places?
    Search and replace in files throughout a directory!
  * Fixed bugs stay fixed!
    Version control with git works fantastically for R markdown files.
* Start your export and watch the automagic happen!

`library(examiner)` packages code which has served three educators and thousands of students for many years, including several seasons of individualized home exams during the COVID-19 pandemic.

(Update: `examiner` can now [create Rmd files from your WISEflow multiple choice questions](articles/wiseflow_to_examiner.html)!)

## A single R markdown source code for three export formats

```{r, echo = FALSE}
gal <- tibble::tribble(
  ~heading, ~description, ~png,
  "R markdown", "a single source for three output formats!", "source.jpg",
  "HTML", "Quick single-file export", "test_knit.jpg",
  "Canvas", "Learning management system", "canvas.jpg",
  "WISEflow", "Exam system", "wiseflow.jpg"
) |> 
  dplyr::mutate(i = dplyr::row_number() - 1,
                active = i == 0) |> 
  purrr::transpose()

readr::read_file("carousel_template.html") |> 
  whisker::whisker.render() |> 
  htmltools::HTML()
```

## Extra goodies

`examiner` includes a couple of helpful RStudio addins.

![](man/figures/addins.png)

<details>
<summary>
***Machine translate question files between bokmål and nynorsk***
</summary>
:::{.emphasized-paragraph}
This addin translates quiz and question files between bokmål and nynorsk forms of Norwegian using [Apertium](https://www.apertium.org).
Apertium supports many more languages, but unfortunately there is as yet no "language pair" between English and Norwegian.
(English to Spanish is included in `examiner` as a demo.)

Usage:
Once you select a question file, it will be translated and saved to a new file, which opens in RStudio so you can try knitting it and check whether the translated file still works.
If you select a quiz file, all its question files will get translated too.

[Video: Translating a quiz from bokmål to nynorsk](https://arken.nmbu.no/~jonvi/examiner_translate.mp4){target="_blank}

Apertium works very well for body text and tries not to interfere with markdown syntax.
It may occasionally make unwanted changes to R code, so make sure to test the translated file and tweak it if necessary.

I highly recommend installing a visual diff tool which integrates into your file manager.
Then you can sort files by name so that `question_1_nno.Rmd` comes below `question_1.Rmd`, select the two files, right-click and diff.

* KDiff3: [KDiff3 homepage](https://invent.kde.org/sdk/kdiff3), [KDiff3 downloads](https://download.kde.org/stable/kdiff3/?C=M;O=D)).
* [WinMerge](https://winmerge.org) (Windows only)
* [TortoiseGit](https://tortoisegit.org) (Windows only) is a visual git client which includes a diff tool.
:::
</details>

<details>
<summary>
***Import multiple-choice questions from WISEflow***
</summary>
:::{.emphasized-paragraph}
If you have existing Assignments or Content banks in WISEflow, you can export a JSON file with the questions.
See [Converting a WISEflow export to examiner files](articles/wiseflow_to_examiner.html).
:::
</details>

## Frequently asked questions

<details>
<summary>
***What does it look like in practice?***
</summary>
:::{.emphasized-paragraph}
All the files are R markdown.
A *quiz file* holds the quiz description and lists the *question files* in its YAML header.
This video shows knitting of the "Canvas gallery" example you can make in RStudio with `New File > R Markdown > From template` once you have installed `library(examiner)`.
Knitting the quiz file will knit each of the question files in turn, using the Canvas API to add questions to the quiz.

```{r echo = FALSE}
vembedr::embed_youtube("6cBMdR2tYW4")
```
:::
</details>

<details>
<summary>
***Why not just edit within Canvas or WISEflow?***
</summary>
:::{.emphasized-paragraph}
You certainly can -- it's probably what you're doing already.
However, **editing in Canvas or WISEflow is slow and error-prone if your exercises include computer code and resulting figures and tables**.
With R markdown, you can rest assured that what the student sees resulted exactly from your data and your code.
With that comes several advantages:

**You stand on firm footing.**
Runnable examples included with `library(examiner)` verify a well-defined set of HTML features you can rely on.
If it works on your computer, it will work in Canvas and WISEflow too.

**You stay in control.**
WISEflow offers zero portability to or from other systems.
Even its own export omits images and only links to images on a WISEflow server.
Canvas does claim to support a number of import and export formats, but in practice it only works for trivial text-based exercises.
See the FAQ about [portability](#portability) below.

**You can use power tools such as version control, search and replace across files, ...**
Large-scale search and replace and general checking is cumbersome in Canvas and WISEflow as you click between items, sections and feedback boxes.
Keeping copies of your work takes longer than if you use version control.
:::
</details>

<details>
<summary>
***What do I need to get started?***
</summary>
:::{.emphasized-paragraph}
You need R and RStudio, of course.

Skills-wise, you need only be able to knit an R markdown file that is given to you, and modify it further.

Once you have installed the `examiner` package, the New Project dialog will offer a new project type called "Quizzes using examiner".
The dialog for New File > R markdown > From template will let you create new files for each question type, as well as a skeleton for a new quiz.

To use Canvas, you need Teacher permissions in a course.

To use WISEflow, you need login access to https://wiseflow.net.
If you want to include images generated from R markdown, you will additionally need access to a web server.

Follow the "Installation" section below for a detailed walkthrough.
:::
</details>

<details id="portability">
<summary>
***But surely Canvas and WISEflow offer import and export??***
</summary>
:::{.emphasized-paragraph}
They claim to do so, but it sucks.

WISEflow offers very few export options:
You can print a pdf.
Or you can export an undocumented JSON format used by no other systems, and where all your images remain locked inside the WISEflow server.
Gone is the code that generated all the goodness, if you were authoring questions with R markdown to begin with.

(Good news: `examiner` can [create Rmd files from your WISEflow multiple choice questions](articles/wiseflow_to_examiner.html).)

Canvas claims to support several formats including the Question and Test Interoperability standard, but in practice it *does not work*.
Canvas stays on the obsolete 1.2 version which supports few question types, and makes undocumented custom tweaks to the format.
No demo files are available to verify functionality before switching systems.

It is astonishing that nobody in the purchase process demanded to witness a successful roundtrip export and import for all available question types before making the deal.
:::
</details>

## Installation

You need to have R and RStudio installed.
Then `install.packages("remotes")` if you don't already have the `remotes` library.
Next, install `examiner` using this command: 

``` r
remotes::install_gitlab("jonovik/examiner", dependencies = TRUE)
```

**Restart the R session** using RStudio's "Session" menu to make available the `examiner` templates for a course project, quiz file or question file.

Then read the [Get started](articles/examiner.html) guide.
Happy quizzing!
